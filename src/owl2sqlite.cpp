#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

#include "cowl.h"
#include "negationNormalForm.h"
#include <sqlite3.h>

/*
  Returns a pointer to the axiom table that contains the liste axioms and two tables of classes and axioms 
*/
sqlite3* load_axioms_to_sqlite(list<CowlAnyAxiom*> axs)
{ 
   sqlite3 *arrows;
   char *requete_sql=((char *)(malloc(8192)));
   char *db_err_msg=((char *)(NULL));
   int  db_retval ;
   db_retval = sqlite3_open(":memory:", &arrows); 
   if (db_retval!=SQLITE_OK) 
   {
          fprintf(stderr, "Code Erreur SQL :  %d\n", db_retval); 
          fprintf(stderr, "Erreur SQL : 0 %s\n", db_err_msg); 
   } 
   strcpy(requete_sql,"CREATE TABLE IF NOT EXISTS classes ");
   strcat(requete_sql," (idCls INT AUTO_INCREMENT, class TEXT, ");
   strcat(requete_sql," PRIMARY KEY (idCls) );");
   db_retval=sqlite3_exec(arrows,requete_sql,NULL,NULL,&db_err_msg);
   if (db_retval!=SQLITE_OK) 
   {
      fprintf(stderr, "Code Erreur SQL :  %d\n", db_retval); 
      fprintf(stderr, "Erreur SQL : 1 %s\n", db_err_msg); 
      sqlite3_free(db_err_msg); 
      free(requete_sql); 
      return NULL; 
   }
   
   strcpy(requete_sql,"CREATE TABLE IF NOT EXISTS axioms ");
   strcat(requete_sql," (idAx INT AUTO_INCREMENT, left INT, right INT, ");
   strcat(requete_sql," PRIMARY KEY (idAx), ");
   strcat(requete_sql," FOREIGN KEY  (left) REFERENCES classes(idCls), ");
   strcat(requete_sql," FOREIGN KEY  (right) REFERENCES classes(idCls) );");
   db_retval=sqlite3_exec(arrows,requete_sql,NULL,NULL,&db_err_msg);
   if (db_retval!=SQLITE_OK) 
   {
      fprintf(stderr, "Erreur SQL : 2 %s\n", db_err_msg); 
      sqlite3_free(db_err_msg); 
      free(requete_sql);  
      return NULL; 
   }
   
   
   
   list<CowlAnyAxiom*>::iterator it  ;
   int idCls = 0;
   int idAx = 0;
   const char *left,  *right ;
   int no =  1;
   for (it = axs.begin(); it != axs.end(); it++)
   {
      //cout << "no =" << ++no <<"\n";
      int noAx =  0;
      if (cowl_axiom_get_type(*it) == COWL_AT_SUB_CLASS )
      {
           left = cowl_string_get_cstring( cowl_to_string( cowl_sub_cls_axiom_get_sub ( (CowlSubClsAxiom*) *it)) );
           right = cowl_string_get_cstring( cowl_to_string( cowl_sub_cls_axiom_get_super ( (CowlSubClsAxiom*) *it)) );
           noAx = 1;
     
      } else if (cowl_axiom_get_type(*it) == COWL_AT_DISJ_CLASSES || cowl_axiom_get_type(*it) == COWL_AT_EQUIV_CLASSES  )
      {
          CowlVector * v =  cowl_nary_cls_axiom_get_classes ( (CowlNAryClsAxiom *) *it );
          //int n = (int)cowl_vector_count(v);
          //for(int i=0; i++; i <  2)
          left =   cowl_string_get_cstring( cowl_to_string(  (CowlClsExp*)cowl_vector_get_item(v, (ulib_uint)0  ) ) );
          right=   cowl_string_get_cstring( cowl_to_string(  (CowlClsExp*)cowl_vector_get_item(v, (ulib_uint)1  ) ) );   
          noAx = 1;
      } 
          
      if(noAx==0)
         continue;
      std::string left2 = std::to_string(++idCls);
      strcpy(requete_sql,"INSERT INTO  classes (class)  VALUES ( '");
      strcat(requete_sql,  left);
      strcat(requete_sql,  "' ); " );
      
      db_retval=sqlite3_exec(arrows,requete_sql,NULL, NULL,&db_err_msg);
      if (db_retval!=SQLITE_OK) 
      {
          fprintf(stderr, "Erreur SQL : 3 %s\n", db_err_msg); 
          fprintf(stderr, "Query: 3 %s\n", requete_sql); 
          sqlite3_free(db_err_msg); 
          free(requete_sql); 
          return NULL; 
      }
       std::string right2 = std::to_string(++idCls);
      strcpy(requete_sql,"INSERT INTO  classes (class) VALUES ( '");
      strcat(requete_sql,  right );
      strcat(requete_sql,  "'); " );
      db_retval=sqlite3_exec(arrows,requete_sql,NULL, NULL,&db_err_msg);
      if (db_retval!=SQLITE_OK) 
      {
          fprintf(stderr, "Erreur SQL : 4 %s\n", db_err_msg); 
          fprintf(stderr, "Query: 4 %s\n", requete_sql);
          sqlite3_free(db_err_msg); 
          free(requete_sql); 
          return NULL; 
      }
      
      strcpy(requete_sql,"INSERT INTO  axioms (left, right) VALUES ( ");
      strcat(requete_sql,  left2.c_str()  );
      strcat(requete_sql,  ", " );
      strcat(requete_sql,  right2.c_str() );
      strcat(requete_sql,  ");" );
      db_retval=sqlite3_exec(arrows,requete_sql,NULL, NULL,&db_err_msg);
      if (db_retval!=SQLITE_OK) 
      {
          fprintf(stderr, "Erreur SQL : 5 %s\n", db_err_msg); 
          fprintf(stderr, "Query: 5 %s\n", requete_sql);
          sqlite3_free(db_err_msg); 
          free(requete_sql); 
          return NULL; 
      }
   }      
    sqlite3_free(db_err_msg); 
    free(requete_sql); 
   return arrows;
}
