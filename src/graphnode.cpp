#include "cowl.h"
#include "graphnode.h"
#include "negationNormalForm.h"
#include "research.h"

#include <algorithm>
#include <iostream>
#include <list>


using namespace std;

/* Graph Node Definition and Methods*/

list<CowlClsExp*> Node::getLabel()
{
    return this->Label;
}

list<CowlClsExp*>::iterator Node::getIterator()
{
    return this->Label.begin();
}

list<CowlClsExp*>::iterator Node::getLast()
{
    return this->Label.end();
}

void Node::createLabel(list<CowlClsExp*> new_list)
{
    this->Label = new_list; 
}

void Node::updateLabel(CowlClsExp* new_concept)
{
    this->Label.emplace_back(new_concept);
}
/* Edge Definition and Methods*/

Edge::Edge(Node* parent, Node* child)
{
    this->parent = parent;
    this->child = child;
}


/*Graph Definition and Methods*/

Graph::Graph(CowlClsExp* base_concept, list<CowlSubClsAxiom*> list_axiom){

    CowlClass* top = cowl_class_from_static("owl:Thing");
    list<CowlClsExp*> list_axiom_nnf = {};

    list<CowlSubClsAxiom*>::iterator axiom_ptr = list_axiom.begin();

    while( axiom_ptr != list_axiom.end() )
    {

	CowlSubClsAxiom* current_axiom = *axiom_ptr; 

	// Recovering the upper class
	CowlClsExp* axiom_sup = cowl_sub_cls_axiom_get_super(current_axiom);

	// Recovering the complement of the sub class in NNF
	CowlClsExp* axiom_sub_nnf = nnf((CowlClsExp*) cowl_obj_compl(
				    cowl_sub_cls_axiom_get_sub(current_axiom))
					);

	// Creation of the axiom disjunction neg E sqcup F
	UVec(CowlObjectPtr) ops = uvec(CowlObjectPtr);
	uvec_push(CowlObjectPtr, &ops, axiom_sub_nnf);
	uvec_push(CowlObjectPtr, &ops, axiom_sup);

	CowlVector* operands = cowl_vector(&ops);

	list_axiom_nnf.push_back((CowlClsExp*) cowl_nary_bool(COWL_NT_UNION, operands));

	cowl_release(operands);

	axiom_ptr++;
    }

    // Putting the result as the list of neg E sqcup F
    this->formatted_axioms = list_axiom_nnf;

    // Creation of the first Label
    Node* v_0 = new Node();

    // adding the formattid axioms
    v_0->createLabel(list_axiom_nnf);
    // adding the base concept
    v_0->updateLabel(base_concept);
    // adding top concept
    v_0->updateLabel((CowlClsExp*) top);
    // there are no other label in the chained list
    v_0->Next = NULL;

    // We point the head of the graph to v_0 as the first member of the Chained list
    *(this->head) = v_0;
    //this->current = v_0; // Not sure I have to keep that one

    // No edges
    list<Edge> edges = {};
    this->edges = edges;
}

//equivalent to push : vraiment
void Graph::new_node(Node* parent_node, CowlClsExp* exist_filler, CowlClsExp* univ_filler) 
{
    /*
    CowlClass* top = cowl_class_from_static("owl:Thing");
    // We create the new node
    Node* new_node = new Node();
    // we add the disjunction axioms
    new_node->createLabel(this->formatted_axioms);
    // we add top
    new_node->updateLabel((CowlClsExp)* top);
    // adding the exist filler
    new_node->updateLabel(exist_filler);

    if(univ_filler != NULL)
    {
	new_node->updateLabel(univ_filler);
    }
    new_node->Next = *(this->head);
    *(this->head) = new_node;
    */

} 

Node** Graph::getHead(){ return this->head; }
//Node* Graph::getCurrent(){ return this->current; }
list<CowlClsExp*> Graph::getAxioms(){return this->formatted_axioms; }


//Graph Graph::extend(Node* current_node, list<CowlClsExp*> axioms){}
/*
Je pense que ça va devoir se mettre ailleurs que dans graph

current_node = current_graph->getHead();
while(current_node != NULL){ // Tant qu'il y a des nodes à traiter
    current_concept = current_node->getIterator();
    while(current_concept != current_node->getLast())
    {
	CowlClsExp* objExp = *current_concept;	
	
	int type = (int) cowl_get_type(objExp);

	switch(type)
	{
	    case COWL_OT_CE_OBJ_INTERSECT:  // Conjunction
	    {
		// recovering all concepts/operands in one vector 
		CowlVector* operands_vector = 
				    cowl_nary_bool_get_operands((CowlNAryBool*) ObjExp);

		// recovering the number of concepts in the conjunction
		int nb_of_operands = (int) cowl_vector_count(operands_vector);

		// adds all conjunct of a conjunction
		for(int i = 0; i < nb_of_operands; i++)
		{ 
		    CowlClsExp* temp = (CowlClsExp*) cowl_vector_get_item(operands_vector, i);

		    // Check if the concept isn't already in the Label
		    if (checkConcept(current_node->getLabel(), temp) == false )
		    {
			// if it's not, we add it
			current_node->updateLabel(temp);
		    }
		    cowl_release(temp);
		}
		
		// We go to the next concept
		current_concept++;
	    }

	    case COWL_OT_CE_OBJ_UNION:	   // Disjunction
	    {

		current_concept++;
	    }
	    case COWL_OT_OBJ_SOME:	    // Existential restriction
	    {

		list<CowlClsExp*> Label = current_node->getLabel(); 
		CowlClsExp* exist_filler = 
				cowl_obj_quant_get_filler( (CowlObjQuant*) ObjExp);

		// Check if we can find at least *one* universal restriction with
		// the same role as the existential restriction
		CowlClsExp* UnivObj = checkUnviversal(Label, ObjExp);

		if(UnivObj != NULL)
		{
		    // if universal object has been found
		    CowlClsExp* univ_filler =
				cowl_obj_quant_get_filler( (CowlObjQuant*) UnivObj);

		    // If both fillers are never both present in the same label
		    if (!checkFillers(current_graph->getHead(), exist_filler,  univ_filler))
			current_graph->new_node(exist_filler, univ_filler);
		    } 
		}else{
		    // There are no universal object with the same role
		    // as the existnetial object
		    if (checkFiller(current_graph->getHead(), exist_filler) == false)
		    {
			// if we haven't found any label that contains the filler,
			// then we create a new node
			current_graph->new_node(exist_filler, NULL);
		    }
		}

		current_concept++;
	    }
	    case COWL_OT_OBJ_ALL:	    // Universal restriction
	    {
		// We do nothing, Universal Restriction are only dealt with 
		// in combinatation with an Existential Restriction

		current_concept++;
	    }
	    case COWL_OT_CE_CLASS:	    // If it's a concept name
	    {
		// We do nothing
		current_concept++;
	    }

	}

    }
    current_node = current_node->Next;

}
*/
