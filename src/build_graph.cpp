#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sqlite3.h>
#include "cowl.h"
//#include "negationNormalForm.h"
#include "build_graph.h"
#include "load_ontology.h"

using namespace std;

UOStream *std_out = uostream_std();

void update_edges(Graph* g, UVec(CowlObjectPtr)* current_node, UVec(CowlObjectPtr)* new_node)
{
    // If the table already contains the key
    // TODO :	la fonction uhash_contains provoque un segmentation fault dès que la clef
    //		a été ajouté préalablement
    if(uhash_contains(CowlObjectTable, g->edges, current_node))
    {
	// we need to update the children of this node
	UVec(CowlObjectPtr)* children = (UVec(CowlObjectPtr)*) uhmap_get(CowlObjectTable, 
					    g->edges, current_node, NULL);

	uvec_push(CowlObjectPtr, children, new_node);
	uhmap_replace(CowlObjectTable, g->edges, current_node, children, NULL);

    }else{

	uhash_put(CowlObjectTable, g->edges, current_node, NULL);
	// we create the vector that contains the values i.e. children of the node
	UVec(CowlObjectPtr)* children = (UVec(CowlObjectPtr)*)malloc(sizeof(*children));
	*children = uvec(CowlObjectPtr);

	uvec_push(CowlObjectPtr, children, new_node);

	// we create the key associated with the node
	uhmap_add(CowlObjectTable, g->edges, current_node, children, NULL);
    }

}

// Formatting general axioms (unabsorbed) into neg E \sqcup F
// for every general axiom.
void format_genAxiom(UHash(CowlObjectTable)* genAxioms, UVec(CowlObjectPtr)* n_0)
{

    if(genAxioms != NULL)
    {
	uhash_foreach(CowlObjectTable, genAxioms, ax)
	{
	    //CowlClsExp *l =(CowlClsExp*)cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)*ax.key);
	    //CowlClsExp *r =(CowlClsExp*)cowl_sub_cls_axiom_get_super((CowlSubClsAxiom*)*ax.key);
	    CowlClsExp *l_c = (CowlClsExp *) cowl_obj_compl( (CowlClsExp*) *ax.key );  
	    UVec(CowlObjectPtr) ops = uvec(CowlObjectPtr); 
	    uvec_push(CowlObjectPtr, &ops, nnf(l_c));
	    UVec(CowlObjectPtr) conjs = uvec(CowlObjectPtr); 
	    UVec(CowlObjectPtr)*val = (UVec(CowlObjectPtr)*)*ax.val;
	    uvec_foreach(CowlObjectPtr, val, conj)
	    {
	     uvec_push(CowlObjectPtr, &conjs, *conj.item);
	    }
	    CowlNAryBool *cons = cowl_nary_bool(COWL_NT_INTERSECT, cowl_vector(&conjs));
	    uvec_push(CowlObjectPtr, &ops, (CowlClsExp*)cons); 
	    CowlVector *operands = cowl_vector(&ops);
	    CowlNAryBool *uni = cowl_nary_bool(COWL_NT_UNION, operands);
	    uvec_push(CowlObjectPtr, n_0, uni);
	    uvec_deinit(CowlObjectPtr, &ops);
	    uvec_deinit(CowlObjectPtr, &conjs);
	    cowl_release(operands);
	}
    }
}

void create_node(Graph* g, UVec(CowlObjectPtr)* current_node, CowlClsExp* exists_filler, 
CowlClsExp* universal_filler, UHash(CowlObjectTable)* axioms ,  UHash(CowlObjectTable)* genAxioms )
{
	// Creating the new node
	UVec(CowlObjectPtr)* new_node = 
			(UVec(CowlObjectPtr)*)malloc(sizeof(*new_node));

	*new_node = uvec(CowlObjectPtr);
	uvec_push(CowlObjectPtr, new_node, exists_filler);	

	if(universal_filler != NULL)
	    uvec_push(CowlObjectPtr, new_node, universal_filler);	

	

	// absorbing concept from axioms
	get_n_targets(axioms, new_node);

	// adding general axioms
	format_genAxiom(genAxioms, new_node);

	// adding the node to the graph
	uvec_push(CowlObjectPtr, g->nodes, new_node);
	
	// adding the edges
	// TODO : débugger le segmentation fault
	//update_edges(g, current_node, new_node);
}

// Only an existential object has been found
void exists_adding(Graph* g, UVec(CowlObjectPtr)* current_node,
CowlClsExp* exists_filler, UHash(CowlObjectTable)* axioms, UHash(CowlObjectTable)* gen_axioms)
{

    // checking if the filler is already in any anode
    bool proceed = true;
    uvec_foreach(CowlObjectPtr, g->nodes, node)
    {
	UVec(CowlObjectPtr)* loop_node = 
			    (UVec(CowlObjectPtr)*) *node.item;

	if(uvec_contains(CowlObjectPtr, loop_node, exists_filler))
	{
	    proceed = false;
	    break;
	}
    }

    if(proceed)
    {
	// creating new node
	create_node(g, current_node,  exists_filler, (CowlClsExp*)NULL, axioms, gen_axioms);
    }
}

// an existential object has been found and one universal object as well
void exists_universal_adding(Graph* g, UVec(CowlObjectPtr)* current_node,
CowlClsExp* exists_filler, UHash(CowlObjectTable)* axioms, UHash(CowlObjectTable)* genAxioms,  int universal_index)
{
    CowlObjQuant* universal_obj = (CowlObjQuant*) 
		    uvec_get(CowlObjectPtr, current_node, universal_index);

    CowlClsExp* universal_filler = (CowlClsExp*) cowl_obj_quant_get_filler(universal_obj);

    // checking if the fillers are in any nodes
    bool proceed = true;
    uvec_foreach(CowlObjectPtr, g->nodes, node)
    {
	UVec(CowlObjectPtr)* loop_node = (UVec(CowlObjectPtr)*) *node.item;

	if(uvec_contains(CowlObjectPtr, loop_node, exists_filler)
	&& uvec_contains(CowlObjectPtr,  loop_node, universal_filler))
	{
	    proceed = false;
	    break;
	}
    }

    if(proceed)
    {
	// creating new node
	create_node(g, current_node, exists_filler, universal_filler, axioms, genAxioms);
    }
}

// a universal object has been found and it there's also an existential object present
void universal_exists_adding(Graph* g, UVec(CowlObjectPtr)* current_node,
CowlClsExp* universal_filler, UHash(CowlObjectTable)* axioms, UHash(CowlObjectTable)* genAxioms, int exists_index)
{
    CowlObjQuant* exists_obj = (CowlObjQuant*)
		uvec_get(CowlObjectPtr, current_node, exists_index);

    CowlClsExp* exists_filler = (CowlClsExp*)cowl_obj_quant_get_filler(exists_obj);

    // checking if the fillers are in any nodes
    bool proceed = true;
    uvec_foreach(CowlObjectPtr, g->nodes, node)
    {
	UVec(CowlObjectPtr)* loop_node = (UVec(CowlObjectPtr)*) *node.item;

	if(uvec_contains(CowlObjectPtr, loop_node, exists_filler)
	&& uvec_contains(CowlObjectPtr,  loop_node, universal_filler))
	{
	    proceed = false;
	    break;
	}
    }

    if(proceed)
    {
	// creating new node
	create_node(g, current_node, exists_filler, universal_filler, axioms, genAxioms);
    }
}


//It retursns true if there is a clash in a node
bool check_clash(UVec(CowlObjectPtr)* node)
{
    uvec_foreach(CowlObjectPtr, node, k_clss)
    {
      CowlAny* ele_1 = (CowlAny*)*k_clss.item;
      int type_1 = (int) cowl_get_type(ele_1);
//cout << "NOTHING = " << cowl_write_string(std_out, cowl_to_string( (CowlAny*)getNothing() )) <<  "\n";
//cout << "ELE_1 = " << cowl_write_string(std_out, cowl_to_string( (CowlAny*)ele_1 )) <<  "\n";
      if(cowl_equals(ele_1, (CowlAny*)getNothing()))
{

//cout << "*********************************clash nothing\n";
         return true;
}
      for(ulib_uint i = k_clss.i + 1; i < uvec_count(CowlObjectPtr, node); ++i)
      {
            CowlAny* ele_2 = (CowlAny*)uvec_get(CowlObjectPtr, node, i);

            //cout << "ELE_2 = " << cowl_write_string(std_out, cowl_to_string( (CowlAny*)ele_2 )) <<  "\n";
            
//cout << "ELE_2 = " << cowl_write_string(std_out, cowl_to_string( (CowlAny*)ele_2 )) <<  "\n";

             int type_2 = (int) cowl_get_type(ele_2);
             CowlClsExp *oper;
             if(type_1==COWL_OT_CE_OBJ_COMPL && type_2==COWL_OT_CE_CLASS)
             {
                oper = cowl_obj_compl_get_operand((CowlObjCompl*)ele_1);
                if(cowl_equals(oper, ele_2))
{
//cout << "***********************************clash 1\n";
                   return true;
}

             } else if(type_2==COWL_OT_CE_OBJ_COMPL && type_1==COWL_OT_CE_CLASS)
             {
                oper = cowl_obj_compl_get_operand((CowlObjCompl*)ele_2);
                if(cowl_equals(oper, ele_1))
{
//cout << "*************************************clash 2\n";
                   return true;
}
             }
        }
    }
    return false;
 }

/*
  Get all Y such that  res => ... => Y where "res" is a set
*/
void get_n_targets(UHash(CowlObjectTable) *axioms, UVec(CowlObjectPtr)*res)
{
   bool stop =false;
   UVec(CowlObjectPtr) curr = uvec(CowlObjectPtr);
   uvec_copy(CowlObjectPtr, res, &curr);
   UVec(CowlObjectPtr) next;//
   while(!stop)
   {
	 stop = true;
	 next = uvec(CowlObjectPtr);
	 uvec_foreach(CowlObjectPtr, &curr, curr_k)
	 {
	   if(uhash_contains(CowlObjectTable, axioms, *curr_k.item))
	   {
	    UVec(CowlObjectPtr)*val = (UVec(CowlObjectPtr)*)uhash_value(CowlObjectTable, axioms, 
		    uhash_get(CowlObjectTable, axioms, *curr_k.item));
	    uvec_foreach(CowlObjectPtr, val, key_1)
	    {
	      if(!uvec_contains(CowlObjectPtr, &curr, *key_1.item))
	      {
		    uvec_push(CowlObjectPtr, &next,  *key_1.item);
		    uvec_push(CowlObjectPtr, res,  *key_1.item);
		    stop = false;
	      }
	    }
	   }
	 }
	 if(!stop)
	 {
	   uvec_deinit(CowlObjectPtr, &curr);
	   curr = uvec(CowlObjectPtr);
	   uvec_copy(CowlObjectPtr, &next, &curr);
	   uvec_deinit(CowlObjectPtr, &next);
	 } else
	 {
	   uvec_deinit(CowlObjectPtr, &curr);
	   uvec_deinit(CowlObjectPtr, &next);
	 }
  }
}

/*
  "begin" : the index of the node in "nodes" to be processed
  "clash" = true iff there is a clash
*/
Graph* saturate_graph(Graph* g, int begin, bool* clash, UHash(CowlObjectTable)* axioms, UHash(CowlObjectTable)* genAxioms)
{
    // Saturation starts here
    bool graph_changed = true;
    bool has_clash = false;

    /* Where we need to start the saturation, begin = 0 the first call of saturate;

    begin >= 0, the node where we need to start saturating again after the addtion of 
		a disjunct in is_satisfiable function
    */
    int node_index = begin;

    /* To access the new nodes created by an intermediate node,
    we need to take an extra step in the operands of the vector
    
    Say if there are 4 nodes, and we start at the 2nd one, the new node
    will be created after the 4th ones ( 4 = number of nodes + 1 = (last index + 1) + 1) ;
    so we need to access the 5th nodes from the 2nd one, so we need to go 
    from the index 1 to the index 4 : node_step = 3 = 4 - 1 = number of nodes - begin
    */ 
    int node_step = (int)uvec_count(CowlObjectPtr, g->nodes) - begin;

    /*
     vector where we keep the subconcepts that are to be added 
     and those that needs to be checked
    */
    UVec(CowlObjectPtr) to_be_added = uvec(CowlObjectPtr);
    UVec(CowlObjectPtr) to_be_treated = uvec(CowlObjectPtr);

    while(graph_changed)
    {
	UVec(CowlObjectPtr)* current_node = (UVec(CowlObjectPtr)*) 
					uvec_get(CowlObjectPtr, g->nodes, node_index);

	has_clash = check_clash(current_node);
	if(has_clash)
	{
	    *clash = has_clash;
	    return g;
	}
	// index to determine correctly which concepts are to be treated
	// Idea : pass an argument from build_graph to determine from which element 
	// we should start treating the element
	int start_index = 0;
	// Determine whether we keep checking concepts for a label or not
	bool to_be_changed = true;
       
	while(to_be_changed)
	{
	    to_be_treated = uvec_get_range_from(CowlObjectPtr, 
			    current_node, start_index);

	    // applying rules/saturating per the algorhtim
	    uvec_foreach(CowlObjectPtr, &to_be_treated, current_concept)	
	    {
//cowl_write_string(std_out, cowl_to_string(*current_concept.item));
		// Seperating the case depending
		// on the type of each concepts
		switch(cowl_get_type(*current_concept.item))
		{
		    case COWL_OT_CE_OBJ_INTERSECT:  // Conjunction
		    {
			// We need to recover all conjuncts of the conjunction
			CowlVector* operands_vector = 
			cowl_nary_bool_get_operands((CowlNAryBool*) *current_concept.item);


			// We add them to the label	
			cowl_vector_foreach( operands_vector, conjunct)
			{
			    // adds the conjunct only if it's not already there
			    if(!(uvec_contains(CowlObjectPtr, current_node, 
								    *conjunct.item)))
			    {
				uvec_push_unique(CowlObjectPtr, &to_be_added, 
					(CowlClsExp*) *conjunct.item);
			    }
			}

			break;
		    }

		    case COWL_OT_CE_OBJ_SOME:	    // Existential restriction
		    {
			CowlObjPropExp* exists_role = cowl_obj_quant_get_prop(
					(CowlObjQuant*) *current_concept.item);

			CowlClsExp* exists_filler = cowl_obj_quant_get_filler(
					(CowlObjQuant*) *current_concept.item);

			int universal_index = -1; 

			uvec_foreach(CowlObjectPtr, current_node, candidate)
			{
//cowl_write_string(std_out, cowl_to_string(*candidate.item));
//owl2manchester(*current_concept.item);
//cout << "\n";
//display_graph(g);
			    // Looking for every possible universal concepts
			    if(cowl_get_type(*candidate.item) == COWL_OT_CE_OBJ_ALL)
			    {
				// Checking if they have the same rolename
				if(cowl_obj_quant_get_prop(
					(CowlObjQuant*) *candidate.item) == exists_role)
				{
				    // Index that'll help us recover the found universal
				    // object in the create_node function
				    universal_index = (int) uvec_index_of(CowlObjectPtr,
						current_node, *candidate.item);

				    exists_universal_adding(g, current_node,
					exists_filler, axioms, genAxioms, universal_index);
				}
			    }

			}
//cout << "autre exists\n";
			// if no universal object sharing role name, has been found
			if(universal_index < 0)  			
			{
			    exists_adding(g, current_node, exists_filler, axioms, genAxioms);
			}
			break;
		    } 

		    case COWL_OT_CE_OBJ_ALL:	    
		    {
			CowlObjPropExp* universal_role = cowl_obj_quant_get_prop(
					(CowlObjQuant*) *current_concept.item);

			CowlClsExp* universal_filler = cowl_obj_quant_get_filler(
					(CowlObjQuant*) *current_concept.item);

			uvec_foreach(CowlObjectPtr, current_node, candidate)
			{
			    // Looking for every possible existential concepts
			    if(cowl_get_type(*candidate.item) == COWL_OT_CE_OBJ_SOME)
			    {
				// Checking if they have the same rolename
				if(cowl_obj_quant_get_prop(
					(CowlObjQuant*) *candidate.item) == universal_role)
				{
				    int exists_index = (int) uvec_index_of(CowlObjectPtr,
						current_node, *candidate.item);

				    universal_exists_adding(g, current_node,
					universal_filler, axioms, genAxioms, exists_index);
				}
			    } 
			}

			break;
		    }
		    //case COWL_OT_CE_OBJ_UNION: case treated in the meta loop
		    //case COWL_OT_CE_CLASS: 
		    //case COWL_OT_CE_OBJ_COMPL: 
		    default:
		    {
			break;
		    }
		}
	    }
	
	    // adding every object from the axiom that subsume the object that will be added
	    get_n_targets(axioms, &to_be_added);

	   

	    if(uvec_count(CowlObjectPtr, &to_be_added) != 0)
	    {
		// If there are other subconcepts to be treated, they are added right after 
		// the previous last concepts
		start_index = uvec_index_of(CowlObjectPtr, current_node, 
			    uvec_last(CowlObjectPtr, current_node)) + 1;

		// adding the new subconcepts to the label
		uvec_append(CowlObjectPtr, current_node, &to_be_added);
    
		has_clash = check_clash(current_node);
		if(has_clash)
		{
		    *clash = has_clash;
		    return g;
		}

		// reinitialising to_be_added vector to nothing 
		uvec_remove_all(CowlObjectPtr, &to_be_added);
		uvec_shrink(CowlObjectPtr, &to_be_added);
	    }else{
		to_be_changed = false;

		has_clash = check_clash(current_node);
		if(has_clash)
		{
		    *clash = has_clash;
		    return g;
		}
	    }

	} // End of while(to_be_changed), we have treated everything in the current_node
	
	if(node_step != 1)
	{
	    /* This means we're treating a node that is not the first (or the last) node
	    of the graph */

	    node_index = node_index + node_step;
    
	    /* once we have the index of the newest node, the other newer node will follow
	    this one directly one step further in the node vector*/
	    node_step = 1;
	}else{
	    /* If we're treating the first node ever or the last of a previous graph,
	    or the following of newer nodes, then we just need to take one step further*/
	    node_index++;
	}

	// If there are no next node, then we stop
	if(node_index >= uvec_count(CowlObjectPtr, g->nodes))
	{
	    graph_changed = false;

	    has_clash = check_clash(current_node);
	    if(has_clash)
	    {
		*clash = has_clash;
		return g;
	    }
	}
    }

    return g;
}

/*
   This function builds a new graph from "g" for a non-deterministic case (C OR D) with
   + "disjunct_node_index" indicates the node containing (C OR D)
   + "disjunctConcept" \in {C, D}
   + "clash" will be filled by  "saturate_graph"
*/
Graph* build_graph(Graph* g, int disjunct_node_index, CowlClsExp* disjunctConcept, bool* clash,
UHash(CowlObjectTable)* axioms, UHash(CowlObjectTable)* genAxioms)
{
    // We recover the node where a disjunct was detected
    UVec(CowlObjectPtr)* disjunct_node = (UVec(CowlObjectPtr)*) uvec_get(CowlObjectPtr,
						g->nodes, disjunct_node_index);

    // We add the disjunct in the designated node
    uvec_push(CowlObjectPtr, disjunct_node, disjunctConcept);

    // We saturate the graph again starting from this node and all the 
    // new node the processure might have created
    g = saturate_graph(g, disjunct_node_index, clash, axioms, genAxioms);

    return g;
}

/*
   Dataset for check if there are transitive arrows between A_i and B_j
   A => B, A1 => A, A2 => A, A3 => A1 and A2,  
   B=> B1 and B2, B1=> B3, B2=> B3
*/
/*UHash(CowlObjectTable)* generate_axioms_trans()
{
  CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
  CowlClass* class_b = cowl_class_from_static("http://ontology.com#B");
  CowlClass* class_a1 = cowl_class_from_static("http://ontology.com#A_1");
  CowlClass* class_a2 = cowl_class_from_static("http://ontology.com#A_2");
  CowlClass* class_a3 = cowl_class_from_static("http://ontology.com#A_3");
  CowlClass* class_b1 = cowl_class_from_static("http://ontology.com#B_1");
  CowlClass* class_b2 = cowl_class_from_static("http://ontology.com#B_2");
  CowlClass* class_b3 = cowl_class_from_static("http://ontology.com#B_3");


  UHash(CowlObjectTable)* axioms_1 = (UHash(CowlObjectTable)*)malloc(sizeof(*axioms_1));
  *axioms_1=uhmap(CowlObjectTable);

  UVec(CowlObjectPtr) *ab = (UVec(CowlObjectPtr)*)malloc(sizeof(*ab));
  *ab = uvec(CowlObjectPtr);
  uvec_push(CowlObjectPtr, ab, class_b);
  //A => B
  uhmap_add(CowlObjectTable, axioms_1, class_a, ab, NULL);

  UVec(CowlObjectPtr) *a1a = (UVec(CowlObjectPtr)*)malloc(sizeof(*a1a));
  *a1a = uvec(CowlObjectPtr);
  uvec_push(CowlObjectPtr, a1a, class_a);
  
  //A1 => A
  uhmap_add(CowlObjectTable, axioms_1, class_a1, a1a, NULL);

  UVec(CowlObjectPtr) *a2a = (UVec(CowlObjectPtr)*)malloc(sizeof(*a2a));
  *a2a = uvec(CowlObjectPtr);
  uvec_push(CowlObjectPtr, a2a, class_a);
  //A2 => A
  uhmap_add(CowlObjectTable, axioms_1, class_a2, a2a, NULL);

  UVec(CowlObjectPtr) *a3a12 = (UVec(CowlObjectPtr)*)malloc(sizeof(*a3a12));
  *a3a12 = uvec(CowlObjectPtr);
  uvec_push(CowlObjectPtr, a3a12, class_a1);
  uvec_push(CowlObjectPtr, a3a12, class_a2);
  //A3 => A1 and A2
  uhmap_add(CowlObjectTable, axioms_1, class_a3, a3a12, NULL);


  UVec(CowlObjectPtr) *bb12 = (UVec(CowlObjectPtr)*)malloc(sizeof(*bb12));
  *bb12 = uvec(CowlObjectPtr);
  uvec_push(CowlObjectPtr, bb12, class_b1);
  uvec_push(CowlObjectPtr, bb12, class_b2);
  //B=> B1 and B2
  uhmap_add(CowlObjectTable, axioms_1, class_b, bb12, NULL);

  UVec(CowlObjectPtr) *b1b3= (UVec(CowlObjectPtr)*)malloc(sizeof(*b1b3));
  *b1b3 = uvec(CowlObjectPtr);
  uvec_push(CowlObjectPtr, b1b3, class_b3);
  //B1=> B3
  uhmap_add(CowlObjectTable, axioms_1, class_b1, b1b3, NULL);

  UVec(CowlObjectPtr) *b2b3 = (UVec(CowlObjectPtr)*)malloc(sizeof(*b2b3));
  *b2b3 = uvec(CowlObjectPtr);
  uvec_push(CowlObjectPtr, b2b3, class_b3);
   //B2=> B3
  uhmap_add(CowlObjectTable, axioms_1, class_b2, b2b3, NULL);

  return axioms_1;
}
*/

void test_saturate_graph(const char* filename)
{
    Axioms_load* axiom_load = load_ontology(filename);
    Graph* g = (Graph*)malloc(sizeof(Graph));

    UVec(CowlObjectPtr)* nodes = (UVec(CowlObjectPtr)*)malloc(sizeof(*nodes));
    *nodes = uvec(CowlObjectPtr);

    UHash(CowlObjectTable)* edges = (UHash(CowlObjectTable)*)malloc(sizeof(*edges));
    *edges = uhmap(CowlObjectTable);
    UVec(CowlObjectPtr) stack = uvec(CowlObjectPtr);

    UVec(CowlObjectPtr)* n_0 = (UVec(CowlObjectPtr)*)malloc(sizeof(*n_0));
    *n_0 = uvec(CowlObjectPtr);

    if(axiom_load != NULL)
    {
	CowlClass* initConcept = cowl_class_from_static("http://www.ontology.com#A");


	uvec_push(CowlObjectPtr, n_0, initConcept);
	uvec_push(CowlObjectPtr, nodes, n_0);

	get_n_targets(axiom_load->axioms, n_0);

	g->nodes = nodes;
	g->edges = edges;

	bool clash;
	g = saturate_graph(g, 0, &clash, axiom_load->axioms, axiom_load->genAxioms);
	display_graph(g);
	    
    }else{
	CowlClass* initConcept = cowl_class_from_static("http://www.ontology.com#A");

	UHash(CowlObjectTable)* list_axiom = generate_axioms_bg_1();


	uvec_push(CowlObjectPtr, n_0, initConcept);
	uvec_push(CowlObjectPtr, nodes, n_0);

	get_n_targets(list_axiom, n_0);

	g->nodes = nodes;
	g->edges = edges;

	bool clash;
	g = saturate_graph(g, 0, &clash, list_axiom, NULL);
	display_graph(g);
    }
}
UHash(CowlObjectTable)* generate_axioms_bg_2()
{
    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_c = cowl_class_from_static("http://ontology.com#C");
    CowlClass* class_d = cowl_class_from_static("http://ontology.com#D");

    UVec(CowlObjectPtr) ops_1 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_1, class_c);
    uvec_push(CowlObjectPtr, &ops_1, class_d);

    CowlVector* operands_1 = cowl_vector(&ops_1);

    CowlNAryBool* union_CD = cowl_nary_bool(COWL_NT_UNION, operands_1);
    cowl_release(operands_1);

    UVec(CowlObjectPtr)* sup_A = (UVec(CowlObjectPtr)*)malloc(sizeof(*sup_A));
    *sup_A = uvec(CowlObjectPtr);

    uvec_push(CowlObjectPtr, sup_A, union_CD);

    UHash(CowlObjectTable)* list_axiom = (UHash(CowlObjectTable)*)malloc(sizeof(*list_axiom));
    *list_axiom=uhmap(CowlObjectTable);

    uhmap_add(CowlObjectTable, list_axiom, class_a, sup_A, NULL);

    return list_axiom;
}

UHash(CowlObjectTable)* generate_axioms_bg_1()
{
    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_c = cowl_class_from_static("http://ontology.com#C");
    CowlClass* class_d = cowl_class_from_static("http://ontology.com#D");
    CowlClass* class_e = cowl_class_from_static("http://ontology.com#E");
    CowlClass* class_f = cowl_class_from_static("http://ontology.com#F");
    CowlClass* class_h = cowl_class_from_static("http://ontology.com#H");
    CowlClass* nothing = cowl_class_from_static("owl:Nothing");

    CowlObjProp* prop_r = cowl_obj_prop_from_static("http://ontology.com#R");

    // E sqcap F sqcap H
    UVec(CowlObjectPtr) ops_1 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_1, class_e);
    uvec_push(CowlObjectPtr, &ops_1, class_f);
    uvec_push(CowlObjectPtr, &ops_1, class_h);

    CowlVector* operands_1 = cowl_vector(&ops_1);

    CowlNAryBool* inter_EFH = cowl_nary_bool(COWL_NT_INTERSECT, operands_1);
    cowl_release(operands_1);

    // exist R.C, R.E
    CowlObjQuant* exists_RC = cowl_obj_quant(COWL_QT_SOME, prop_r , class_c);
    CowlObjQuant* exists_RE = cowl_obj_quant(COWL_QT_SOME, prop_r , class_e);

    // forall R.D, R.F, R.H

    CowlObjQuant* forall_RD = cowl_obj_quant(COWL_QT_ALL, prop_r , class_d);
    CowlObjQuant* forall_RF = cowl_obj_quant(COWL_QT_ALL, prop_r , class_f);
    CowlObjQuant* forall_RH = cowl_obj_quant(COWL_QT_ALL, prop_r , class_h);

    // RC sqcap RD
    UVec(CowlObjectPtr) ops_2 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_2, exists_RC);
    uvec_push(CowlObjectPtr, &ops_2, forall_RD);

    CowlVector* operands_2 = cowl_vector(&ops_2);
    CowlNAryBool* inter_RCRD = cowl_nary_bool(COWL_NT_INTERSECT, operands_2);
    cowl_release(operands_2);

    UHash(CowlObjectTable)* list_axiom = (UHash(CowlObjectTable)*)malloc(sizeof(*list_axiom));
    *list_axiom=uhmap(CowlObjectTable);

    // MERGED METHOD
    UVec(CowlObjectPtr)* sup_D = (UVec(CowlObjectPtr)*)malloc(sizeof(*sup_D));
    *sup_D = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, sup_D, exists_RE);
    uvec_push(CowlObjectPtr, sup_D, forall_RF);
    uvec_push(CowlObjectPtr, sup_D, forall_RH);
    //uvec_push(CowlObjectPtr, sup_D, class_d);
    uhmap_add(CowlObjectTable, list_axiom, class_d, sup_D, NULL);
    // ALTERNATE METHOD
    UVec(CowlObjectPtr)* sup_A = (UVec(CowlObjectPtr)*)malloc(sizeof(*sup_A));
    *sup_A = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, sup_A, inter_RCRD);
    uhmap_add(CowlObjectTable, list_axiom, class_a, sup_A, NULL);

/*
    UVec(CowlObjectPtr)* sup_inter_EFH = (UVec(CowlObjectPtr)*)malloc(sizeof(*sup_inter_EFH));
    *sup_inter_EFH = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, sup_inter_EFH, nothing);
    uhmap_add(CowlObjectTable, list_axiom, inter_EFH, sup_inter_EFH, NULL);
*/
    return list_axiom;
}

/*
   This function creates a copy of the nodes and edges "source" without copying the cOWL target objects. It is called from Satisfaible.is_satisfiable
   It maintains the order of elements in "source.nodes"
*/
Graph* copy_graph(Graph* source)
{
    Graph* g = (Graph*)malloc(sizeof(Graph));
    UVec(CowlObjectPtr)* nodes = (UVec(CowlObjectPtr)*)malloc(sizeof(*nodes));
    *nodes = uvec(CowlObjectPtr);
    uvec_foreach(CowlObjectPtr, source->nodes, node)
    {  
       UVec(CowlObjectPtr)* concepts = (UVec(CowlObjectPtr)*)malloc(sizeof(*concepts));
       *concepts = uvec(CowlObjectPtr);
       uvec_copy(CowlObjectPtr, (UVec(CowlObjectPtr)*) *node.item, concepts);
       uvec_push(CowlObjectPtr, nodes, concepts);
    }
    g->nodes = nodes;
    // To do for edges
    UHash(CowlObjectTable)* edges = (UHash(CowlObjectTable)*)malloc(sizeof(*edges));
    *edges = uhmap(CowlObjectTable);
    uhash_copy(CowlObjectTable, source->edges, edges);
    g->edges = edges;
    return g; 
}

/*
   It deallocates just the vector "nodes" and table "edges"
*/
void free_graph(Graph* g)
{
    //We have to deallocate each node since we have used "malloc" for "concepts"
    uvec_foreach(CowlObjectPtr, g->nodes, node)	
    {
       uvec_deinit(CowlObjectPtr, (UVec(CowlObjectPtr)*)(*node.item));
	free(*node.item);
	//cout << "Free 12 " <<  "\n";
    }
    uvec_deinit(CowlObjectPtr, g->nodes);
    free(g->nodes);
    //
    uhash_deinit(CowlObjectTable, g->edges);
    //cout << "Free 2" <<  "\n";
    free(g->edges);
    free(g);
    //cout << "Free 3" <<  "\n";
}

void display_graph(Graph* g)
{
    int int_node = 1;
    uvec_foreach(CowlObjectPtr, g->nodes, node)
    {
	cout << "node " << int_node << "\n";
	UVec(CowlObjectPtr)* clss = (UVec(CowlObjectPtr)*)*node.item;
	uvec_foreach(CowlObjectPtr, clss, cls)
	{
	    char* test2 = owl2manchester((CowlClsExp*) *cls.item);
	    cout << "Class = " << test2 << "\n";
	}  
	int_node++;
    }
}
