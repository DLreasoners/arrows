/*
 *
 * @copyright 
 */
#include <algorithm>
#include <iostream>
#include <list>
#include <ctime>
#include <chrono>
#include <cstring>
#include <sqlite3.h>
#include "cowl.h"
#include "owl2sqlite.h"
#include "utils.h"
#include "saturate.h"
#define ONTO "onto/snomed-ct.owl"

using namespace std;
//UHash(CowlObjectTable) uhash_axioms = uhmap(CowlObjectTable);

//This is the table that stores all axioms from ontology after absorbing
//CowlTable* cowl_table_axioms;
UHash(CowlObjectTable) uhash_axioms;// = uhmap(CowlObjectTable);

//UHash(CowlObjectTable) rev_uhash_axioms = uhmap(CowlObjectTable);
CowlVector* absorb(CowlAny *axiom);
CowlVector* absorb_disj(CowlAny *axiom);
UOStream *std_out_load = uostream_std();

int nbAxEq=0, nbAxSub=0, nbAxDisj=0 ;

/*
 * converts an axiom "X\sqcup Y <= Z" to  "X <= Z" and "Y <= Z"
 */
CowlVector* absorb_disj(CowlAny *axiom)
{
   UVec(CowlObjectPtr) res = uvec(CowlObjectPtr);
   CowlClsExp* l = (CowlClsExp*)cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)axiom);
   CowlClsExp* r = (CowlClsExp*)cowl_sub_cls_axiom_get_super((CowlSubClsAxiom*)axiom);
   CowlVector* classes_c = cowl_nary_bool_get_operands((CowlNAryBool*)l);
   for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(classes_c); ++i)
   {
	 CowlSubClsAxiom *sub_axiom = cowl_sub_cls_axiom((CowlClsExp*)cowl_vector_get_item(classes_c, i), r, NULL);
	 UVec(CowlObjectPtr) v_1 = uvec(CowlObjectPtr);
	 CowlVector* axs = absorb(sub_axiom);//recursion
	 for(ulib_uint i_1 = (ulib_uint)0; i_1 < cowl_vector_count(axs); ++i_1)
	 {
		uvec_push(CowlObjectPtr, &v_1, cowl_vector_get_item(axs, i_1));
	 }
	 for(ulib_uint i_1 = (ulib_uint)0; i_1 <  uvec_count(CowlObjectPtr, &v_1); ++i_1)
	 {
	 	uvec_push(CowlObjectPtr, &res, uvec_get(CowlObjectPtr, (UVec(CowlObjectPtr)*)&v_1, i_1));
	 }
   }
   return (CowlVector*)cowl_vector(&res);
}

/*
 * converts an axiom "X\sqcap Y <= Z" to  "X <= Z \sqcup \neg Y"
 */
CowlVector* absorb_conj(CowlAny *axiom)
{
  UVec(CowlObjectPtr) res = uvec(CowlObjectPtr);
  CowlClsExp* l = (CowlClsExp*)cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)axiom);
  CowlClsExp* r = (CowlClsExp*)cowl_sub_cls_axiom_get_super((CowlSubClsAxiom*)axiom);
  CowlVector* classes_c = cowl_nary_bool_get_operands((CowlNAryBool*)l);
  CowlClsExp* atom=NULL;
  CowlClsExp* disj=NULL;
  for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(classes_c); ++i)
  {
	int type = (int) cowl_get_type(cowl_vector_get_item(classes_c, i));
	if(type == COWL_OT_CE_CLASS || type == COWL_OT_CE_OBJ_COMPL || type == COWL_OT_CE_OBJ_SOME || type == COWL_OT_CE_OBJ_ALL)
	{
	  atom = (CowlClsExp*)cowl_vector_get_item(classes_c, i);
	  break;
	}
	if(type == COWL_OT_CE_OBJ_UNION )
	{
	  disj = (CowlClsExp*)cowl_vector_get_item(classes_c, i);
	}
  }
  if(atom!=NULL)//atomic found
  {
	UVec(CowlObjectPtr) uni = uvec(CowlObjectPtr);
	for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(classes_c) ; ++i)
	{
	  if(!cowl_equals(cowl_vector_get_item(classes_c, i), atom))
	  {
	    CowlClsExp *comple = nnf((CowlClsExp*)cowl_obj_compl(cowl_vector_get_item(classes_c, i)));
	    uvec_push(CowlObjectPtr, &uni,  comple);
	  }
	}
	uvec_push(CowlObjectPtr, &uni, r);
	CowlNAryBool * un = cowl_nary_bool(COWL_NT_UNION, cowl_vector(&uni));
	CowlSubClsAxiom *sub_axiom = cowl_sub_cls_axiom(atom, un, NULL);
	uvec_push(CowlObjectPtr, &res, sub_axiom);
	//if(!uvec_contains(CowlObjectPtr, &classes_load, nnf((CowlClsExp*)atom)))
	//uvec_push(CowlObjectPtr, &classes_load, nnf((CowlClsExp*)atom));
	//if(!uvec_contains(CowlObjectPtr, &classes_load, nnf((CowlClsExp*)un)))
	//uvec_push(CowlObjectPtr, &classes_load, nnf((CowlClsExp*)un));
	return (CowlVector*)cowl_vector(&res);
  } else if(disj!=NULL) //no atomic but disjunction found
  {
	UVec(CowlObjectPtr) uni = uvec(CowlObjectPtr);
	for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(classes_c) && !cowl_equals(cowl_vector_get_item(classes_c, i), disj); ++i)
	{
	   CowlClsExp *comple = nnf((CowlClsExp*)cowl_obj_compl(cowl_vector_get_item(classes_c, i)));
	   uvec_push(CowlObjectPtr, &uni, comple);
	}
	uvec_push(CowlObjectPtr, &uni, r);
	CowlNAryBool * un = cowl_nary_bool(COWL_NT_UNION,  cowl_vector(&uni));
	CowlSubClsAxiom *sub_axiom = cowl_sub_cls_axiom(disj, un, NULL);
	return absorb_disj(sub_axiom);//recursion
  } else
  {
	uvec_push(CowlObjectPtr, &res,  axiom);
	return (CowlVector*)cowl_vector(&res);
  }
}

/*
 * converts an axiom to several axioms whose subsumee is neither a conjunction nor a disjunction
 */
CowlVector* absorb(CowlAny *axiom)
{
	if(cowl_axiom_get_type(axiom) != COWL_AT_SUB_CLASS ) return NULL;
	CowlClsExp* l = (CowlClsExp*)cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)axiom);
	int type = (int) cowl_get_type(l);
	if(type==COWL_OT_CE_OBJ_INTERSECT)
	{
	   return absorb_conj(axiom);
	}
	if(type==COWL_OT_CE_OBJ_UNION)
	{
	   return absorb_disj(axiom);
	} else
	{
	  UVec(CowlObjectPtr) res = uvec(CowlObjectPtr);
	  uvec_push(CowlObjectPtr, &res, axiom);
	  return (CowlVector*)cowl_vector(&res);
	}
}
/*
 * An axiom is stored as a pair (key, value) in "cowl_table_axioms"
 */
void addAxiom2Hashmap(CowlAnyAxiom *ax)
{
	CowlClsExp *l = (CowlClsExp*)cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)ax);
	CowlClsExp *r = (CowlClsExp*)cowl_sub_cls_axiom_get_super((CowlSubClsAxiom*)ax);
	UVec(CowlObjectPtr) *vec = (UVec(CowlObjectPtr)*)malloc(sizeof(*vec));
    *vec = uvec(CowlObjectPtr);
	if((int)cowl_get_type(r)==COWL_OT_CE_OBJ_INTERSECT)
	{
	   CowlVector *ops = (CowlVector *)cowl_nary_bool_get_operands((CowlNAryBool*)r);
	   cowl_vector_foreach(ops, ke)
	   {
		   uvec_push(CowlObjectPtr, vec, *ke.item);
	   }
	}else
	   uvec_push(CowlObjectPtr, vec, r);

	if(uhash_contains(CowlObjectTable, &uhash_axioms, l))
	{
	   UVec(CowlObjectPtr)* val = (UVec(CowlObjectPtr)*)uhmap_get(CowlObjectTable, &uhash_axioms, l, NULL);
	   for(ulib_uint i = 0; i < uvec_count(CowlObjectPtr, vec); ++i)
	   {
	   	  uvec_push(CowlObjectPtr, val, uvec_get(CowlObjectPtr, vec, i));
	   }
	   //uvec_push(CowlObjectPtr, val, r);//bug 11/12/2023
	   free(vec);
	} else
	{
	   //uvec_push(CowlObjectPtr, vec, r);//bug 11/12/2023
	   int ret = uhmap_add(CowlObjectTable, &uhash_axioms, l, vec, NULL);
	}
}

static bool for_each_axiom(cowl_unused void *ctx, CowlAny *axiom)
{
   bool nnfv= true;
   if (cowl_axiom_get_type(axiom) == COWL_AT_SUB_CLASS)
   {
	 //cout << "sub class axiom = " << cowl_write_string(std_out_load, cowl_to_string(cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)axiom))) << "\n";
	  ++nbAxSub;
	 CowlVector *abs = absorb(axiom);
     for(ulib_uint i = 0; i < cowl_vector_count(abs); ++i)
     {
       CowlSubClsAxiom *ax = (CowlSubClsAxiom*)cowl_vector_get_item(abs, i);
       addAxiom2Hashmap(ax);
     }
   } else if (cowl_axiom_get_type(axiom) == COWL_AT_DISJ_CLASSES)
   {
	 ++nbAxDisj;
	 CowlVector *classes_c = cowl_nary_cls_axiom_get_classes((CowlNAryClsAxiom*)axiom);
	 cowl_vector_foreach (classes_c, cls)
	 {
		 //uvec_push(CowlObjectPtr, &classes_load, *cls.item );
	     for(ulib_uint i = cls.i + 1; i < cowl_vector_count(classes_c); ++i)
	     {
	        CowlClsExp *complement = nnf((CowlClsExp*)cowl_obj_compl(cowl_vector_get_item(classes_c, i)));
	        CowlSubClsAxiom *sub_axiom = cowl_sub_cls_axiom(*cls.item, complement, NULL);
	        CowlVector *abs = absorb(sub_axiom);
	        for(ulib_uint i_1 = 0; i_1 < cowl_vector_count(abs); ++i_1)
	        {
	           CowlSubClsAxiom *ax = (CowlSubClsAxiom*)cowl_vector_get_item(abs, i_1);
	           addAxiom2Hashmap(ax);
	        }
	        //if(i==cls.i + 1)
	        //   uvec_push(CowlObjectPtr, &classes_load, complement);
	     }
	 }
   } if(cowl_axiom_get_type(axiom) == COWL_AT_EQUIV_CLASSES)
   {
	   //cout << "cls EQUIV = " << cowl_write_string(std_out_load, cowl_to_string(cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)axiom))) << "\n";
	   ++nbAxEq;
	   CowlVector *classes_c = cowl_nary_cls_axiom_get_classes((CowlNAryClsAxiom*)axiom);
	   CowlClsExp *l = (CowlClsExp*)cowl_vector_get_item(classes_c, 0);
	   CowlClsExp *r = (CowlClsExp*)cowl_vector_get_item(classes_c, 1);
	   //uvec_push(CowlObjectPtr, &axioms_load, cowl_sub_cls_axiom(l,r, NULL));
	   CowlVector *abs = absorb( cowl_sub_cls_axiom(l,r, NULL) );
	   for(ulib_uint i_1 = 0; i_1 < cowl_vector_count(abs); ++i_1)
	   {
		  CowlSubClsAxiom *ax = (CowlSubClsAxiom*)cowl_vector_get_item(abs, i_1);
	   	  //uvec_push(CowlObjectPtr, &axioms_load,  ax);
	   	  addAxiom2Hashmap(ax);
	   	  //cout << owl2manchester(ax) <<"\n";
	   }
	   //uvec_push(CowlObjectPtr, &axioms_load, cowl_sub_cls_axiom(r,l, NULL));
	   abs = absorb( cowl_sub_cls_axiom(r,l, NULL) );
	   for(ulib_uint i_1 = 0; i_1 < cowl_vector_count(abs); ++i_1)
	   {
		  CowlSubClsAxiom *ax = (CowlSubClsAxiom*)cowl_vector_get_item(abs, i_1);
	   	  //uvec_push(CowlObjectPtr, &axioms_load,  ax);
	   	  addAxiom2Hashmap(ax);
	   	  //cout << owl2manchester(ax) <<"\n";
	   }
	   //uvec_push(CowlObjectPtr, &classes_load, l);
	   //uvec_push(CowlObjectPtr, &classes_load, r);
   }
   return true;
}

/*
 * It fills the global uhash_axioms;
 */
Axioms_load* load_ontology(const char *filename)//CowlVector  **atomLeft, CowlVector **genLeft)
{
    cowl_init();
    // Instantiate a manager and deserialize an ontology from file.
    CowlManager *manager = cowl_manager();
    CowlOntology *ontology = cowl_manager_read_path(manager, ustring_wrap(filename, strlen(filename)));//filename
    //top = cowl_class_from_static("owl:Thing");
    //bot = cowl_class_from_static("owl:Nothing");
    if(ontology)
    {
       CowlIterator iter ={NULL, for_each_axiom};
       uhash_axioms = uhmap(CowlObjectTable);
       cowl_ontology_iterate_axioms(ontology, &iter, false);//it builds all axioms and classes and stores into axioms_load and classes_load
       cowl_release(manager);
       cowl_release(ontology);
       cout << "NB EQUI = " << nbAxEq << "\n"; fflush(stdout);
       cout << "NB SUB = " << nbAxSub << "\n"; fflush(stdout);
       cout << "NB DISJ = " << nbAxDisj << "\n"; fflush(stdout);
    } else
    {
       cout  << "Onto NULL Load =" <<"\n";
       return NULL;
    }
    Axioms_load* res = (Axioms_load*)malloc(sizeof(Axioms_load));
    res->axioms = &uhash_axioms;
    res->genAxioms = NULL;
    return  res; //cowl_table_axioms;
}

CowlClass* getThing()
{
	return cowl_class_from_static("owl:Thing");
}

CowlClass* getNothing()
{
	return cowl_class_from_static("owl:Nothing");
}

void test_load()
{
	cowl_init();
	long load1 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	UHash(CowlObjectTable)* axioms =  loadOntology("onto/snomed-ct.owl");
	long load2 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
	if(axioms)
	{
	   int c=0, genAx=0;
	   ulib_uint d=0;
	   cout  << "Number of axioms=" << uhash_count(CowlObjectTable, axioms) << "\n";
	   cout  << "LOAD duration =" << load2 - load1 << " \n";
	   uhash_foreach(CowlObjectTable, axioms, key)
	   {
		 cout << "KEY by Index " << d << "=" << cowl_write_string(std_out_load, cowl_to_string( uhash_key(CowlObjectTable, axioms, d)) )<< "\n";
		 cout << "KEY " << d++ << "=" << cowl_write_string(std_out_load, cowl_to_string(*key.key))<< "\n";
		 c +=  uvec_count(CowlObjectPtr, (UVec(CowlObjectPtr)*)*key.val);
		 int type = (int) cowl_get_type((CowlClsExp*)*key.key);
		 if(type==COWL_OT_CE_OBJ_COMPL)
		 {
			 CowlClsExp *oper = cowl_obj_compl_get_operand((CowlObjCompl*)*key.key);
			 int type_2 = (int) cowl_get_type((CowlClsExp*)oper);
			 if(type_2!=COWL_OT_CE_CLASS)
				 ++genAx;
		 } else
		     if(type!=COWL_OT_CE_CLASS)
		    	 ++genAx;
		 //cout << "KEY = " << cowl_write_string(std_out_load, cowl_to_string(*key.key)) << ", val size = " << uvec_count(CowlObjectPtr, (UVec(CowlObjectPtr)*)*key.val)  << "\n";
		 /*for(ulib_uint i = 0; i < uvec_count(CowlObjectPtr, val); ++i)
		 {
			cout << "VAL = " << cowl_write_string(std_out_load, cowl_to_string( (CowlClsExp*)uvec_get(CowlObjectPtr, val, i) )) << "\n";
		 }*/
	   }
	   cout  << "Nb of general axioms = "  <<  genAx << " \n";
	   cout  << "Total nb supers = "  <<  c << " \n";
    } else
    {
	   cout  << "NULL Axioms"  << " \n";
    }
}

void test_absorb()
{
    UOStream *std_out = uostream_std();
    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_b = cowl_class_from_static("http://ontology.com#B");
    CowlClass* class_c = cowl_class_from_static("http://ontology.com#C");
    CowlObjProp* prop_c = cowl_obj_prop_from_static("http://ontology.com#R");
    CowlObjCompl* neg_a = cowl_obj_compl(class_a);
    UVec(CowlObjectPtr) ops_1 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_1, class_a);
    uvec_push(CowlObjectPtr, &ops_1, class_b);
    uvec_push(CowlObjectPtr, &ops_1, class_c);
    UVec(CowlObjectPtr) ops_2 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_2, class_a);
    uvec_push(CowlObjectPtr, &ops_2, class_b);
    uvec_push(CowlObjectPtr, &ops_2, class_c);
    CowlVector *operands_1 = cowl_vector(&ops_1);
    CowlVector *operands_2 = cowl_vector(&ops_2);
    CowlNAryBool *intersection = cowl_nary_bool(COWL_NT_INTERSECT, operands_1);
    CowlNAryBool *unions = cowl_nary_bool(COWL_NT_UNION, operands_2);
    CowlSubClsAxiom *ax1= cowl_sub_cls_axiom(neg_a, (CowlClsExp*) intersection, NULL);
    char *ch=owl2manchester(ax1);
    cout << "\n Initial Axiom =  " <<  ch <<"\n";
    free(ch);
    CowlVector *test1 = absorb((CowlSubClsAxiom*)ax1);
    for(ulib_uint i = 0; i < cowl_vector_count(test1); ++i)
   	{
    	char* test2 = owl2manchester((CowlSubClsAxiom*)cowl_vector_get_item(test1, i));
    	cout << "\n Absorbed axioms: " << test2 <<"\n";
    	free(test2);
   	}
    ax1= cowl_sub_cls_axiom(intersection, unions, NULL);
    ch=owl2manchester(ax1);
    cout << "\n Initial Axiom =  " <<  ch <<"\n";
    free(ch);
    test1 = absorb((CowlSubClsAxiom*)ax1);
    for(ulib_uint i = 0; i < cowl_vector_count(test1); ++i)
    {
        char* test2 = owl2manchester((CowlSubClsAxiom*)cowl_vector_get_item(test1, i));
        cout << "\n Absorbed axioms: " << test2 <<"\n";
        free(test2);
    }
    ax1= cowl_sub_cls_axiom(unions, intersection,  NULL);
    ch=owl2manchester(ax1);
    cout << "\n Initial Axiom =  " <<  ch <<"\n";
    free(ch);
    test1 = absorb((CowlSubClsAxiom*)ax1);
    for(ulib_uint i = 0; i < cowl_vector_count(test1); ++i)
    {
            char* test2 = owl2manchester((CowlSubClsAxiom*)cowl_vector_get_item(test1, i));
            cout << "\n Absorbed axioms: " << test2 <<"\n";
            free(test2);
    }

    CowlClass* class_d = cowl_class_from_static("http://ontology.com#D");
    ops_1 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_1, class_c);
    uvec_push(CowlObjectPtr, &ops_1, class_d);
    operands_1 = cowl_vector(&ops_1);
    UVec(CowlObjectPtr) ops_3 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_3, class_a);
    uvec_push(CowlObjectPtr, &ops_3, class_b);
    CowlVector * operands_3 = cowl_vector(&ops_3);

    CowlNAryBool *union_2 = cowl_nary_bool(COWL_NT_UNION, operands_1);
    CowlNAryBool *union_3 = cowl_nary_bool(COWL_NT_UNION, operands_3);
    ops_2 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_2, class_a);
    uvec_push(CowlObjectPtr, &ops_2, class_b);
    uvec_push(CowlObjectPtr, &ops_2, union_2);
    operands_2 = cowl_vector(&ops_2);
    CowlNAryBool *intersection_2 = cowl_nary_bool(COWL_NT_INTERSECT, operands_2);
    ax1= cowl_sub_cls_axiom(intersection_2, unions,   NULL);
    ch=owl2manchester(ax1);
    cout << "\n Initial Axiom =  " <<  ch <<"\n";
    free(ch);
    test1 = absorb((CowlSubClsAxiom*)ax1);
    for(ulib_uint i = 0; i < cowl_vector_count(test1); ++i)
    {
       char* test2 = owl2manchester((CowlSubClsAxiom*)cowl_vector_get_item(test1, i));
       cout << "\n Absorbed axioms: " << test2 <<"\n";
       free(test2);
    }
    ops_2 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_2, union_2);
    uvec_push(CowlObjectPtr, &ops_2, union_3);
    operands_2 = cowl_vector(&ops_2);
    intersection_2 = cowl_nary_bool(COWL_NT_INTERSECT, operands_2);
    ax1= cowl_sub_cls_axiom(intersection_2, unions,   NULL);
    ch=owl2manchester(ax1);
    cout << "\n Initial Axiom =  " <<  ch <<"\n";
    free(ch);
    test1 = absorb((CowlSubClsAxiom*)ax1);
    for(ulib_uint i = 0; i < cowl_vector_count(test1); ++i)
    {
        char* test2 = owl2manchester((CowlSubClsAxiom*)cowl_vector_get_item(test1, i));
        cout << "\n Absorbed axioms: " << test2 <<"\n";
        free(test2);
    }


}
