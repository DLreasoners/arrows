#include "cowl.h"
#include "graphnode.h"
#include "negationNormalForm.h"
#include "research.h"

#include <algorithm>
#include <iostream>
#include <list>


using namespace std;

bool checkConcept(list<CowlClsExp*> Label, CowlClsExp* ObjExp)
{
    auto res = find(Label.begin(), Label.end(), ObjExp);
    if (res != Label.end())
    {
	return true;
    }else{
	return false;
    }
}

// Browse all the concept of a Label looking for a universal
// restriction that hsa the same role as the given existential
// restriction
CowlObjQuant* checkUniversal(list<CowlClsExp*> Label, CowlObjQuant* ObjExp)
{
    list<CowlClsExp*>::iterator iter = Label.begin();

    CowlObjPropExp* obj_role = cowl_obj_quant_get_prop(ObjExp);

    while(iter != Label.end()){
	CowlClsExp* candidate  = *iter;
	int type = (int) cowl_get_type(candidate);

	if(type == COWL_OT_CE_OBJ_ALL)
	{
	    CowlObjPropExp* candidate_prop = cowl_obj_quant_get_prop((CowlObjQuant*) candidate);

	    if(candidate_prop == obj_role)
	    {
		return (CowlObjQuant*) candidate;
	    }else{
		iter++;
	    }
	}else{
	    iter++;
	}
    } 
    return NULL;
}

// "Naive" version without edges, but we might do one going out from the current
// node into the one "next" to it (children or parent)
bool checkFiller(Node* head, CowlClsExp* exist_filler)
{ 
    Node* node_ptr = head;

    // if we deal with only an existential object
    while (node_ptr != NULL)
    {
	list<CowlClsExp*> label = node_ptr->getLabel();

	if( checkConcept(label, exist_filler) )
	{
	    return true;
	}else{
	    node_ptr = node_ptr->Next;
	}
    }    

    return false;
}

bool checkFillers(Node* head, CowlClsExp* exist_filler, CowlClsExp* univ_filler)
{
    Node* node_ptr = head;

    while (node_ptr != NULL)
    {
	list<CowlClsExp*> Label = node_ptr->getLabel();

	if(checkConcept(Label, exist_filler))
	{
	    // exists filler is already in a label
	    if(checkConcept(Label, univ_filler))
	    {
		// univ filler is also in the same label
		return true;
	    }else{
		// If not, we check for another label
		node_ptr = node_ptr->Next;
	    }
	}else{
	    // If the existential filler is no in the Label,
	    // the presence or absence of the universal one is 
	    // of no consequence => we check other label 
	    node_ptr = node_ptr->Next;
	}
    }
    return false;
}
















