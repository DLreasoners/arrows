/*
 *
 * @copyright 
 */
#include <algorithm>
#include <iostream>
#include <list>
#include <ctime>
#include <chrono>
#include <cstring>
#include <sqlite3.h>
#include <utility>
#include "cowl.h"
#include "owl2sqlite.h"
#include "utils.h"

using namespace std;
//#define duration(a) std::chrono::duration_cast<std::chrono::nanoseconds>(a).count()
//#define timeNow() std::chrono::high_resolution_clock::now()
//typedef std::chrono::high_resolution_clock::time_point TimeVar;


UOStream *std_out_1 = uostream_std();
 
CowlClsExp* cloningClsExp(CowlClsExp* ObjExp)
{
   int type = (int) cowl_get_type(ObjExp);
   CowlClsExp* res; 
   switch(type)
   {
	    case COWL_OT_CE_CLASS:	 
	    {				 
	        return (CowlClsExp*)cowl_class( cowl_class_get_iri( (CowlClass *)ObjExp ) ) ;
        }
	    case COWL_OT_CE_OBJ_COMPL:
	    {
		   return (CowlClsExp*)cowl_obj_compl ( cloningClsExp ( cowl_obj_compl_get_operand((CowlObjCompl*) ObjExp) ) );
	    }
	    case COWL_OT_CE_OBJ_INTERSECT  :
	    {
	       CowlVector * vv =  cowl_nary_bool_get_operands ( (CowlNAryBool *) ObjExp );
	       UVec(CowlObjectPtr) vec = uvec(CowlObjectPtr);
	       for( ulib_uint i=0; i< cowl_vector_count(vv); i++  ) 
	       {
	            uvec_push(CowlObjectPtr, &vec, cloningClsExp( (CowlClsExp*) cowl_vector_get_item (vv, i) ));
	       } 
	       return  (CowlClsExp*)cowl_nary_bool( COWL_NT_INTERSECT,  cowl_vector(&vec) ); 
	    }
	    
	    case COWL_OT_CE_OBJ_UNION :
	    {
	       CowlVector * vv =  cowl_nary_bool_get_operands ( (CowlNAryBool *) ObjExp );
	       UVec(CowlObjectPtr) vec = uvec(CowlObjectPtr);
	       for( ulib_uint i=0; i< cowl_vector_count(vv); i++  ) 
	       {
	            uvec_push(CowlObjectPtr, &vec, cloningClsExp( (CowlClsExp*) cowl_vector_get_item (vv, i) ));
	       } 
	       return  (CowlClsExp*)cowl_nary_bool( COWL_NT_UNION,  cowl_vector(&vec) ); 
	    }
	     
	    case COWL_OT_CE_OBJ_SOME:		 
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
		
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_ALL, role, 
						    cloningClsExp(filler));

		return result;
	    }
	    case COWL_OT_CE_OBJ_ALL:		// universal restriction
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
	    
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_SOME, role, 
						    cloningClsExp(filler));


		return result;
	    } 
	    
	    default:			 
		return ObjExp;
      }
}

CowlClsExp* nnf(CowlClsExp* ObjExp)
{
  int type = (int) cowl_get_type(ObjExp);
  CowlClsExp* filler=NULL;
  CowlObjPropExp* role= NULL;
  CowlVector* opers = NULL;
  UVec(CowlObjectPtr) new_ops = uvec(CowlObjectPtr);
  if(type==COWL_OT_CE_OBJ_SOME || type == COWL_OT_CE_OBJ_ALL)
  {
     filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
     role   = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
  }
  if(type==COWL_OT_CE_OBJ_INTERSECT || type == COWL_OT_CE_OBJ_UNION)
  {
	 opers = cowl_nary_bool_get_operands((CowlNAryBool*) ObjExp);
  }
  switch(type)
  {
     case COWL_OT_CE_CLASS:
     {
    	  return ObjExp;
     }
     case COWL_OT_CE_OBJ_SOME:
     {
          CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_SOME, role, nnf(filler));
          return result;
     }
     case COWL_OT_CE_OBJ_ALL:
     {
          CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_ALL, role, nnf(filler));
          return result;
     }
     case COWL_OT_CE_OBJ_INTERSECT:
     {
     	  for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(opers); ++i)
     	  {
     		 uvec_push(CowlObjectPtr, &new_ops, nnf((CowlClsExp*)cowl_vector_get_item(opers, i)));
          }
     	  CowlNAryBool *result = cowl_nary_bool(COWL_NT_INTERSECT, cowl_vector(&new_ops));
     	  return (CowlClsExp*)result;
     }
     case COWL_OT_CE_OBJ_UNION:
     {
     	  for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(opers); ++i)
     	  {
     	     uvec_push(CowlObjectPtr, &new_ops, nnf((CowlClsExp*)cowl_vector_get_item(opers, i)));
     	  }
     	  CowlNAryBool *result = cowl_nary_bool(COWL_NT_UNION, cowl_vector(&new_ops));
     	  return (CowlClsExp*)result;
     }
     case COWL_OT_CE_OBJ_COMPL:
     {
	      CowlClsExp* oper = cowl_obj_compl_get_operand((CowlObjCompl*) ObjExp);
	      int type_0 = (int) cowl_get_type(oper);
	      if(type_0==COWL_OT_CE_OBJ_SOME || type_0 == COWL_OT_CE_OBJ_ALL)
	      {
	         filler = cowl_obj_quant_get_filler((CowlObjQuant*) oper);
	         role   = cowl_obj_quant_get_prop((CowlObjQuant*) oper);
	      }
	      if(type_0==COWL_OT_CE_OBJ_INTERSECT || type_0 == COWL_OT_CE_OBJ_UNION)
	      {
	      	 opers = cowl_nary_bool_get_operands((CowlNAryBool*) oper);
	      }
	      switch(type_0)
	      {
	         case COWL_OT_CE_CLASS:
	         {
	        	 return ObjExp;
	         }
	         case COWL_OT_CE_OBJ_COMPL:
	         {
	        	 oper = cowl_obj_compl_get_operand((CowlObjCompl*) oper);
		         return nnf(oper);
	         }
             case COWL_OT_CE_OBJ_SOME:
             {
    	         CowlObjCompl *comple = cowl_obj_compl(filler);
    	         CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_ALL, role, nnf((CowlClsExp*)comple));
    	         return result;
             }
             case COWL_OT_CE_OBJ_ALL:
             {
                 CowlObjCompl *comple = cowl_obj_compl(filler);
     	         CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_SOME, role, nnf((CowlClsExp*)comple));
     	         return result;
             }
	         case COWL_OT_CE_OBJ_INTERSECT:
	         {
		         for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(opers); ++i)
		         {
		            CowlObjCompl *comple = cowl_obj_compl(cowl_vector_get_item(opers, i));
		        	CowlClsExp* term = nnf((CowlClsExp*)comple);
		            uvec_push(CowlObjectPtr, &new_ops, term);
		         }
		         CowlNAryBool *result = cowl_nary_bool(COWL_NT_UNION, cowl_vector(&new_ops));
		         return (CowlClsExp*)result;
	         }
	         case COWL_OT_CE_OBJ_UNION:
	         {
	        	 for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(opers); ++i)
	        	 {
	        	 	CowlObjCompl *comple = cowl_obj_compl(cowl_vector_get_item(opers, i));
	        	 	CowlClsExp* term = nnf((CowlClsExp*)comple);
	        	 	uvec_push(CowlObjectPtr, &new_ops, term);
	        	 }
	        	 CowlNAryBool *result = cowl_nary_bool(COWL_NT_INTERSECT, cowl_vector(&new_ops));
	        	 return (CowlClsExp*)result;
	         }
	         default :
	         {
	        	 return ObjExp;
	         }
	      }
     }
	 default :
	 {
	     return ObjExp;
	 }
   }
}

char* owl2manchester(CowlClsExp *ObjExp)
{
	UOStream *std_out = uostream_std();
	//int init_size = 8;
	char* res = NULL; //(char*) calloc(1, sizeof(char));
	int type = (int) cowl_get_type(ObjExp);
	switch(type)
	{
	   case COWL_OT_CE_CLASS:
	   {
		   char* clsName = cowl_string_release_copying_cstring (cowl_to_string(ObjExp));
		   char *last = strrchr(clsName, '#');
		   char *last2 = strrchr(clsName, '/');
		   if(last!=NULL)
		   {
		   	   clsName = ++last;
		   } else if(last2!=NULL)
		   {
		   	   clsName = ++last2;
		   }
		   res = (char*)calloc(strlen(clsName) + 1, sizeof(char));
		   strcat(res,clsName);
		   break;
	   }
	   case COWL_OT_CE_OBJ_COMPL:
	   {
		  CowlClsExp *oper = cowl_obj_compl_get_operand((CowlObjCompl*)ObjExp);
		  char* clsName = owl2manchester(oper);
		  //cout << "clsName=" << clsName <<"\n";
		  res = (char*)calloc(strlen("(not ") + strlen(clsName)+ strlen(")")+1, sizeof(char));
		  strcat(res, "(not ");
		  strcat(res, clsName);
		  strcat(res, ")");
		  //cout << "res len 2=" << strlen(res) <<"\n";
		  break;
	   }
	   case COWL_OT_CE_OBJ_SOME:
	   {
		  CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		  CowlObjPropExp* role= cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
		  char* clsName = owl2manchester(filler);
		  char* roleName = cowl_string_release_copying_cstring (cowl_to_string(role));
		  char *last = strrchr(roleName, '#');
		  char *last2 = strrchr(roleName, '/');
		  if(last!=NULL)
		  {
		  	   		 roleName = ++last;
		  } else if(last2!=NULL)
		  {
		  	   		 roleName = ++last2;
		  }
		  res = (char*)calloc( strlen(" (") + strlen(roleName) + strlen(" some ") + strlen(clsName)+  strlen(")")+1,
				              sizeof(char));
		  strcat(res, " (");
		  strcat(res, roleName);
		  strcat(res, " some ");
		  strcat(res, clsName);
		  strcat(res, ")");
		  break;
	   }
	   case COWL_OT_CE_OBJ_ALL:
	   {
	   	  CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
	   	  CowlObjPropExp* role= cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
	   	  char* clsName = owl2manchester(filler);
	   	  char* roleName = cowl_string_release_copying_cstring (cowl_to_string(role));
	   	  char *last = strrchr(roleName, '#');
	   	  char *last2 = strrchr(roleName, '/');
	   	  if(last!=NULL)
	   	  {
	   		 roleName = ++last;
	   	  } else if(last2!=NULL)
		  {
	   		 roleName = ++last2;
		  }
	   	  res = (char*)calloc(strlen(" (") + strlen(roleName) + strlen(" only ") + strlen(clsName)+  strlen(")")+1, sizeof(char));
	   	  strcat(res, " (");
	   	  strcat(res, roleName);
	   	  strcat(res, " only ");
	   	  strcat(res, clsName);
	   	  strcat(res, ")");
	   	  break;
	   }
	   case COWL_OT_CE_OBJ_INTERSECT:
	   {
		  CowlVector *opers = cowl_nary_bool_get_operands((CowlNAryBool*) ObjExp);
		  res = (char*)calloc(1, sizeof(char));
		  for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(opers); ++i)
		  {
			 CowlClsExp *term = (CowlClsExp*)cowl_vector_get_item(opers, i);
			 char* clsName = owl2manchester(term);

			 if(i==0)
			 {
			    res = (char*)realloc(res, strlen(res) + strlen("(") + strlen(clsName) +1);
			    strcat(res, "(");
			    strcat(res, clsName);
			 }
			 else
			 {
			 	res = (char*)realloc(res, strlen(res) + strlen(" and ") + strlen(clsName)+1);
			 	strcat(res, " and ");
			 	strcat(res, clsName);
			 }
		  }
		  res = (char*)realloc(res, strlen(res)   + strlen(")")+ 1);
		  strcat(res, ")");
		  break;
	   }
	   case COWL_OT_CE_OBJ_UNION:
	   {
		  CowlVector *opers = cowl_nary_bool_get_operands((CowlNAryBool*) ObjExp);
		  res = (char*)calloc(1, sizeof(char));
		  for(ulib_uint i = (ulib_uint)0; i < cowl_vector_count(opers); ++i)
		  {
		   	 CowlClsExp *term = (CowlClsExp*)cowl_vector_get_item(opers, i);
		   	 char* clsName = owl2manchester(term);
		   	 if(i==0)
		   	 {
		   	    res = (char*)realloc(res, strlen(res) + strlen("(") + strlen(clsName)+1);
		   	    strcat(res, "(");
		   	    strcat(res, clsName);
		   	 }
		   	 else
		   	 {
		   		res = (char*)realloc(res, strlen(res) + strlen(" or ") + strlen(clsName)+1);
		   		strcat(res, " or ");
		   	    strcat(res, clsName);
		   	 }
		  }
		  res = (char*)realloc(res, strlen(res) +   strlen(")")+ 1);
		  strcat(res, ")");
		  break;
	   }
	   default :
	   {
		   char* clsName = cowl_string_release_copying_cstring (cowl_to_string(ObjExp));
		   res = (char*)calloc(strlen(clsName)+ strlen(" ") +1, sizeof(char));
		   strcat(res, " ");
		   strcat(res, clsName);
	   }
    }

	return res;
}

char* owl2manchester(CowlAny *axiom)
{
  //int init_size = 8;
  char* lc=NULL;
  char* rc=NULL;
  char* res = (char*) calloc(1, sizeof(char));

  if (cowl_axiom_get_type(axiom) == COWL_AT_SUB_CLASS)
  {
	CowlClsExp *l = (CowlClsExp*)cowl_sub_cls_axiom_get_sub((CowlSubClsAxiom*)axiom);
	CowlClsExp *r = (CowlClsExp*)cowl_sub_cls_axiom_get_super((CowlSubClsAxiom*)axiom);
	lc =  owl2manchester(l);
	rc =  owl2manchester(r);
	res = (char*)realloc(res, strlen(lc) + strlen(" <= ") + strlen(rc) + 1);
	res =  strcat(res, lc);
	res =  strcat(res, " <= ");
	res =  strcat(res, rc);
  } else  if(cowl_axiom_get_type(axiom) == COWL_AT_EQUIV_CLASSES)
  {
	  CowlVector *classes_c = cowl_nary_cls_axiom_get_classes((CowlNAryClsAxiom*)axiom);
	  CowlClsExp *l = (CowlClsExp*)cowl_vector_get_item(classes_c, 0);
	  CowlClsExp *r = (CowlClsExp*)cowl_vector_get_item(classes_c, 1);
	  lc =  owl2manchester(l);
	  rc =  owl2manchester(r);
	  res = (char*)realloc(res, strlen(lc) + strlen(" = ") + strlen(rc) + 1);
	  res =  strcat(res, lc);
	  res =  strcat(res, " = ");
	  res =  strcat(res, rc);
  } else
  {
	 res = cowl_string_release_copying_cstring(cowl_to_string(axiom));
  }

  return res;
}
/*CowlClassExp* manchester2Owl(char* str)
{

}*/

void test_owl2manchesterAxiom()
{
    UOStream *std_out = uostream_std();
    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_b = cowl_class_from_static("http://ontology.com#B");
    CowlObjProp* prop_c = cowl_obj_prop_from_static("http://ontology.com#R");
    CowlObjCompl* neg_a = cowl_obj_compl(class_a);
    UVec(CowlObjectPtr) ops = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops, class_a);
    uvec_push(CowlObjectPtr, &ops, class_b);
    uvec_push(CowlObjectPtr, &ops, class_b);
    CowlVector *operands = cowl_vector(&ops);
    CowlNAryBool *intersection = cowl_nary_bool(COWL_NT_INTERSECT, operands);
    CowlSubClsAxiom *ax1= cowl_sub_cls_axiom(neg_a,  intersection, NULL);
    char* test1 = owl2manchester((CowlSubClsAxiom*)ax1);
    cout << "\n Initial = " <<  cowl_string_release_copying_cstring(cowl_to_string(ax1)) <<"\n";
    cout << "\n Sub class : " << test1 <<"\n";
    free(test1);
    CowlNAryClsAxiom *ax2 = cowl_nary_cls_axiom(COWL_NAT_EQUIV, operands, NULL);
    char* test2 = owl2manchester((CowlNAryClsAxiom*)ax2);
    cout << "\n Initial =  " <<  cowl_string_release_copying_cstring(cowl_to_string(ax2)) <<"\n";
    cout << "\n Equiv class : " << test2 <<"\n";
    free(test2);
}

void test_owl2manchester()
{
    UOStream *std_out = uostream_std();
    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_b = cowl_class_from_static("http://ontology.com#B");
    CowlObjProp* prop_c = cowl_obj_prop_from_static("http://ontology.com#R");
    CowlObjCompl* neg_a = cowl_obj_compl(class_a);
    char* test1 = owl2manchester((CowlClsExp*)neg_a);
    cout << "\n Initial = " <<  cowl_string_release_copying_cstring(cowl_to_string(neg_a)) <<"\n";
    cout << "\n Negation : " << test1 <<"\n";
	free(test1);
	CowlObjQuant* exists = cowl_obj_quant(COWL_QT_SOME, prop_c , class_a);
	char* test2 = owl2manchester((CowlClsExp*)exists);
	cout << "\n Initial = " <<  cowl_string_release_copying_cstring(cowl_to_string(exists)) <<"\n";
	cout << "\n Exists : " << test2 <<"\n";
    free(test2);

    UVec(CowlObjectPtr) ops = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops, class_a);
    uvec_push(CowlObjectPtr, &ops, class_b);
    uvec_push(CowlObjectPtr, &ops, class_b);
    CowlVector *operands = cowl_vector(&ops);
    CowlNAryBool *intersection = cowl_nary_bool(COWL_NT_INTERSECT, operands);
    char* test3 = owl2manchester((CowlClsExp*)intersection);
    cout << "\n Initial = " <<  cowl_string_release_copying_cstring(cowl_to_string(intersection)) <<"\n";
    cout << "\n Inter : " << test3 <<"\n";
    free(test3);

    CowlObjQuant* forall = cowl_obj_quant(COWL_QT_ALL, prop_c , class_a);
    char* test4 = owl2manchester((CowlClsExp*)forall);
    cout << "\n Initial = " <<  cowl_string_release_copying_cstring(cowl_to_string(forall)) <<"\n";
    cout << "\n Forall : " << test4 <<"\n";
    free(test4);

    ops = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops, class_a);
    uvec_push(CowlObjectPtr, &ops, class_b);
    uvec_push(CowlObjectPtr, &ops, class_b);
    operands = cowl_vector(&ops);
    CowlNAryBool *unions = cowl_nary_bool(COWL_NT_UNION, operands);
    char* test5 = owl2manchester((CowlClsExp*)unions);
    cout << "\n Initial = " <<  cowl_string_release_copying_cstring(cowl_to_string(unions)) <<"\n";
    cout << "\n Union : " << test5 <<"\n";
    free(test5);
}

void test_nnf()
{
    UOStream *std_out = uostream_std();
    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_b = cowl_class_from_static("http://ontology.com#B");
    CowlObjProp* prop_c = cowl_obj_prop_from_static("http://ontology.com#R");

    CowlObjCompl* neg_a = cowl_obj_compl(class_a);

    UVec(CowlObjectPtr) ops = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops, class_a);
    uvec_push(CowlObjectPtr, &ops, class_b);

    UVec(CowlObjectPtr) ops_1 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_1,  cowl_obj_compl(class_a));
    uvec_push(CowlObjectPtr, &ops_1, cowl_obj_compl(class_b));

    //CowlObjQuant* exists = cowl_obj_quant(COWL_QT_SOME, prop_c , class_a);
    CowlVector *operands = cowl_vector(&ops);

    CowlVector *operands_1 = cowl_vector(&ops_1);

    // Create the CowlNAryBool object (an intersection in this case) and release the vector.
    CowlNAryBool *intersection = cowl_nary_bool(COWL_NT_INTERSECT, operands);

    cout << "\nIntersection : \n ";
    cowl_write_string(std_out, cowl_to_string(intersection));


    CowlNAryBool *intersection_1 = cowl_nary_bool(COWL_NT_INTERSECT, operands_1);

    cout << "\nIntersection_1 : \n ";
    cowl_write_string(std_out, cowl_to_string(intersection_1));

    CowlClsExp* nnf_intersect_1 = nnf((CowlClsExp*) intersection_1);

    cout << "\nNnf_intersection_1 : \n ";
    cowl_write_string(std_out, cowl_to_string(nnf_intersect_1));


    CowlObjCompl* neg_intersect_1 = cowl_obj_compl(intersection_1);


    cout << "\nNeg_intersection_1 : \n ";
    cowl_write_string(std_out, cowl_to_string(neg_intersect_1));

    CowlObjCompl* neg_intersect_2 = cowl_obj_compl(neg_intersect_1);

    cout << "\nNeg_neg_intersection_1 : \n ";
    cowl_write_string(std_out, cowl_to_string(neg_intersect_2));

    CowlClsExp* neg_intersect_3 = nnf((CowlClsExp*)neg_intersect_2);

    cout << "\n NNF neg_neg_intersection_1 : \n ";
    cowl_write_string(std_out, cowl_to_string(neg_intersect_3));



    CowlObjCompl* neg_intersect = cowl_obj_compl(intersection);

    cout << "\nNeg_intersection : \n ";
    cowl_write_string(std_out, cowl_to_string(neg_intersect));

    CowlClsExp* nnf_neg_intersect = nnf((CowlClsExp*) neg_intersect);

    cout << "\nNnf_neg_intersection : \n";
    cowl_write_string(std_out, cowl_to_string(nnf_neg_intersect));
    cout << "\n";

    CowlClsExp* nnf_intersect = nnf((CowlClsExp*) intersection);

    cout << "\nNnf_intersection : \n";
    cowl_write_string(std_out, cowl_to_string(nnf_intersect));
    cout << "\n";


    CowlObjQuant* exists = cowl_obj_quant(COWL_QT_SOME, prop_c , intersection);
    CowlObjCompl* neg_exists_inter = cowl_obj_compl(exists);
    CowlClsExp* nnf_neg_exists_inter = nnf((CowlClsExp*) neg_exists_inter);

    cout << "\nNeg_exists_inter : \n";
    cowl_write_string(std_out, cowl_to_string(neg_exists_inter));
    cout << "\n";
    cout << "\nNnf_neg_exists_inter  : \n";
    cowl_write_string(std_out, cowl_to_string(nnf_neg_exists_inter));
    cout << "\n";
}

