/*
 *
 * @copyright 
 */
#include <algorithm>
#include <iostream>
#include <list>
#include <ctime>
#include <chrono>
#include <cstring>
#include <sqlite3.h>

#include "cowl.h"
#include "utils.h"
#include "load_ontology.h"
#include "satisfiable.h"
#include "build_graph.h"

using namespace std;

#define ONTO "onto/snomed-ct.owl"

//list<CowlAnyAxiom*> axioms_l;
//list<CowlClsExp*> classes_l;

//UVec(CowlObjectPtr) classes_i = uvec(CowlObjectPtr);
//UVec(CowlObjectPtr) axioms_i = uvec(CowlObjectPtr);
//UVec(CowlObjectPtr) atomicLeft_i = uvec(CowlObjectPtr);
//UVec(CowlObjectPtr) generalLeft_i = uvec(CowlObjectPtr);

//CowlVector *  classes ;
//CowlVector *  axioms  ;
//CowlVector *  atomicLeft;
//CowlVector *  generalLeft;



int atomNb=0;
/* 
  The first element  is pivot
*/
//UVec(CowlObjectPtr) outGoing = uvec(CowlObjectPtr);
//UVec(CowlObjectPtr) inGoing = uvec(CowlObjectPtr);

 
int main(int argc, char *argv[]) {
	cowl_init();
	//test_owl2manchester();
	//test_owl2manchesterAxiom();
       //test_load();

	//Axioms_load* complete_axioms = load_ontology(ONTO);
	//CowlTable *axioms =  cowl_table(axiom_table);

	/*UVec(CowlObjectPtr) v_1= uvec(CowlObjectPtr);
	UVec(CowlObjectPtr) v_2= uvec(CowlObjectPtr);
	CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
	uvec_push(CowlObjectPtr, &v_2, class_a);
	uvec_push(CowlObjectPtr, &v_1, &v_2);
	UVec(CowlObjectPtr)* v_3 = (UVec(CowlObjectPtr)*) uvec_get(CowlObjectPtr, &v_1, 0);
	CowlClass* x= (CowlClass*)uvec_get(CowlObjectPtr, v_3, 0);
	cout << owl2manchester(x) <<"\n";*/
	//test_get_n_sources_targets(axioms);
	//test_get_trans(axioms);

	//test_is_trans(axioms);
	//CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
	


	// argv[1] = path to ontology, argv[2] = initial concept
	// Utilisation : bin/arrows <argv[1]> <argv[2]>
	test_satisfiable(argv[1]);

	
	//bool sat = is_satisfiable((CowlClsExp*)class_a, complete_axioms->axioms, complete_axioms->genAxioms);

	//test_is_clash_trans(axioms);
    //test_absorb();
    /*UHash(CowlObjectTable) classes_hash = uhmap(CowlObjectTable);
    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_b = cowl_class_from_static("http://ontology.com#B");
    UVec(CowlObjectPtr) v= uvec(CowlObjectPtr);
    //UVec(CowlObjectPtr) v1= uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &v, class_a);
    uvec_push(CowlObjectPtr, &v, class_b);
    //uvec_push(CowlObjectPtr, &v1, class_b);
    UVec(CowlObjectPtr) *v2;
    uhmap_set(CowlObjectTable, &classes_hash, class_a, &v, (CowlAny**)&v2);

    CowlTable * h = (CowlTable*)cowl_table(&classes_hash);
    CowlAny *r_val = cowl_table_get_value(h, class_a);*/
    return EXIT_SUCCESS;
}
