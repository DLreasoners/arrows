#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

#include "cowl.h"
#include "negationNormalForm.h"

using namespace std;

// Ivano: if you're returning references to Cowl objects, make sure to always return
// retained instances (which you must later release), otherwise it's impossible to
// know if a returned object must or must not be released by calling code.
// By looking at the code it looks like you are already doing that, so take it as
// a heads up to save you headaches down the line :)
CowlClsExp* nnf(CowlClsExp* ObjExp, bool neg)
{
    // If the class expression is a negation or coming from a negation
    int type = (int) cowl_get_type(ObjExp);

    // Negative branch
    if(type == COWL_OT_CE_OBJ_COMPL || neg == true)
    {
	// Depending on the type of the class expression,
	// we need to change the constructor
	switch(type){
	    case COWL_OT_CE_OBJ_COMPL:
	    {
		// If this is the first time we enter a negative branch or
		// if it comes from a positive branch
		if(!neg)
		{
		    return (CowlClsExp*) nnf(
			    cowl_obj_compl_get_operand((CowlObjCompl*) ObjExp), true);
		// We are on a negative branch, then if there is another negation, then 
		// it becomes a positive branch
		}else{
		    return (CowlClsExp*) nnf(
			    cowl_obj_compl_get_operand((CowlObjCompl*) ObjExp), false);
		} 
	    }
	    case COWL_OT_CE_OBJ_INTERSECT:	// conjunction
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_INTERSECT, true);
	    }
	    case COWL_OT_CE_OBJ_UNION:		// disjunction
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_UNION, true);
	    }
	    case COWL_OT_CE_OBJ_SOME:		// existential restriction  
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
		
		CowlClsExp* nnf_filler = nnf(filler, true);
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_ALL, role, 
						    nnf_filler);

		cowl_release(nnf_filler);
		return result;
	    }
	    case COWL_OT_CE_OBJ_ALL:		// universal restriction
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);

		CowlClsExp* nnf_filler = nnf(filler, true);
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_SOME, role, 
						    nnf_filler);

		cowl_release(nnf_filler);

		return result;
	    } 
	    case COWL_OT_CE_CLASS:	// Stop case in a negative branch, then we just
	    {				// return (CowlClsExp*) the negation of the axioms

		return (CowlClsExp*) cowl_obj_compl(ObjExp);
	    }
	    // DEBUG CASE ?
	    default:			// Not suppose to have any other types than those
	    {				// so we just return (CowlClsExp*) the guilty type and 
					// see what it is
		cout << "There's a problem" << "\n";
		cout << type << "\n";
		return NULL;
	    }
	}

    }else{

	// Positive Branch
	switch(type){
	    case COWL_OT_CE_OBJ_INTERSECT:
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_INTERSECT, false);
	    }
	    case COWL_OT_CE_OBJ_UNION:
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_UNION, false);
	    }
	    case COWL_OT_CE_OBJ_SOME:		// existential restriction  
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
		
		CowlClsExp* nnf_filler = nnf(filler, false);
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_SOME, role, 
						    nnf_filler);

		cowl_release(nnf_filler);
		return result;
	    }
	    case COWL_OT_CE_OBJ_ALL:		// universal restriction
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
		
		CowlClsExp* nnf_filler = nnf(filler, true);
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_ALL, role, 
						    nnf_filler);

		cowl_release(nnf_filler);
		return result;
	    }
	    case COWL_OT_CE_CLASS:	// Stop case normally
	    {
		return (CowlClsExp*) ObjExp;// Since we are in a positive branch, we just 
	    }				    // return (CowlClsExp*) the axiom
	    // DEBUG MODE ?
	    default:			// Not suppose to have any other types than those
	    {				// so we just return (CowlClsExp*) the guilty type and 
					// see what it is
		cout << "There's a problem" << "\n";
		cout << type << "\n";
		return NULL;
	    }
	}

    }
}

CowlClsExp* interUnionCase(CowlClsExp* ObjExp, CowlObjectType type, bool neg)
{
	// recovering all concepts/operands in one vector
	CowlVector* operands_vector = 
			    cowl_nary_bool_get_operands((CowlNAryBool*) ObjExp);
	// recovering the number of concepts in the conjunction
	UVec(CowlObjectPtr) new_ops = uvec(CowlObjectPtr);

	// Ivano: here you could use the cowl_vector_foreach macro like this:
	cowl_vector_foreach (operands_vector, operand) {
	    CowlClsExp *temp = (CowlClsExp*) *operand.item;
	    uvec_push(CowlObjectPtr, &new_ops, nnf(temp, neg));
	}

	CowlVector* new_operands = cowl_vector(&new_ops);

	// Ivano: since the operands are the output of the nnf() function, which returns
	// retained instances, and cowl_vector retains its operands, you must release
	// the operands via e.g.
	cowl_vector_foreach (new_operands, op) {
	     cowl_release(*op.item);
	}

	CowlNAryBool* result;
	
	if(neg == false){		    
	    // we are on a postive branch, we don't change the type
	    // of object
	    if(type == COWL_OT_CE_OBJ_UNION)
	    {	    
		result = cowl_nary_bool(COWL_NT_UNION, new_operands); 
	    }else{
		result = cowl_nary_bool(COWL_NT_INTERSECT, new_operands); 
	    }
	}else{
	    // we are on a negative branch, so we have to change the type based 
	    // on the orignal one
	    if(type == COWL_OT_CE_OBJ_UNION)
	    {	    
		result = cowl_nary_bool(COWL_NT_INTERSECT, new_operands); 
	    }else{
		result = cowl_nary_bool(COWL_NT_UNION, new_operands); 
	    }
	}   

	cowl_release(new_operands);

	return (CowlClsExp*) result;
}
