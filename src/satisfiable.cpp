#include <algorithm>
#include <iostream>
#include <list>
#include <ctime>
#include <chrono>
#include <cstring>
#include <sqlite3.h>

#include "cowl.h"
#include "owl2sqlite.h"
#include "utils.h"
#include "load_ontology.h"
#include "build_graph.h"
#include "satisfiable.h"

using namespace std;

#define ONTO "onto/snomed-ct.owl"


 
/*
  "axioms" is absorbed, i.e each sub is not a conjunction or disjunction
*/
bool is_satisfiable(CowlClsExp* initConcept, UHash(CowlObjectTable)* axioms, UHash(CowlObjectTable)* genAxioms)
{
    Graph* g = (Graph*)malloc(sizeof(Graph));

    UVec(CowlObjectPtr)* nodes = (UVec(CowlObjectPtr)*)malloc(sizeof(*nodes));
    *nodes = uvec(CowlObjectPtr);

    UHash(CowlObjectTable)* edges = (UHash(CowlObjectTable)*)malloc(sizeof(*edges));
    *edges = uhmap(CowlObjectTable);

    UVec(CowlObjectPtr) stack = uvec(CowlObjectPtr);

    UVec(CowlObjectPtr)* n_0 = (UVec(CowlObjectPtr)*)malloc(sizeof(*n_0));
    *n_0 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, n_0, initConcept);
    uvec_push(CowlObjectPtr, nodes, n_0);

    get_n_targets(axioms, n_0);

    g->nodes = nodes;
    g->edges = edges;

   UOStream *std_out_2 = uostream_std();
   int nb_graph = 1;
   bool clash=false;

    format_genAxiom(genAxioms, n_0);

    clash = false;
    g = saturate_graph(g, 0, &clash, axioms, genAxioms);

   nb_graph = 1;
   cout << "Graphe initial\n";
   display_graph(g);

   if(clash)
   {
      free_graph(g);
      cout << "SAT FALSE 0" <<"\n";
      return false;
   }
   
   uvec_push(CowlObjectPtr, &stack, g);
   while( uvec_count(CowlObjectPtr, &stack) > 0 )
   {
         g = (Graph*)uvec_pop(CowlObjectPtr, &stack);
//cout << "Graph POP ========================"  << "\n";
         display_graph(g);
         
         bool saturated = true; //to check if all disjunctions of all nodes  are  processed  
// where is it put to false ? 
         uvec_foreach(CowlObjectPtr, g->nodes, node)
         {  
            UVec(CowlObjectPtr)* n = (UVec(CowlObjectPtr)*) *node.item;
            uvec_foreach(CowlObjectPtr, n, c)
            {
                CowlClsExp *cur =  (CowlClsExp*)*c.item;
                //cout << "LOOP 3 = " <<  cowl_write_string(std_out_2, cowl_to_string(cur)) <<  "\n";
                int t = (int)cowl_get_type(cur);
                //cout << "LOOP 4`" << "\n";
                if( (int)cowl_get_type(cur) == COWL_OT_CE_OBJ_UNION)
                {

//cout << "UNION " << "\n";
                    saturated = false;

//cout << "UNION " << "\n";

                    CowlVector *opers = cowl_nary_bool_get_operands((CowlNAryBool*) cur);
                    bool contained = false;
                    cowl_vector_foreach(opers, op)
                    {
                       //cout << "disjunct = " << cowl_write_string(std_out_2, cowl_to_string(*op.item)) <<  "\n";
                       if(uvec_contains(CowlObjectPtr, n, *op.item) ) // no need to apply OR rule
                       {
                           contained = true;
                           break;
                       }
                    } 
                    if(!contained)
                    {
                       //unsaturated = false; // "g" has a node "n" that needs to be processed
                       cowl_vector_foreach(opers, op)
                       {
                          clash  = false;
nb_graph++;
                          Graph* c_g = copy_graph(g);
// rajouté * devant op.item
                          c_g = build_graph(c_g, (int)node.i, (CowlClsExp*) *op.item, &clash, axioms, genAxioms);
 
cout << "New graph " << nb_graph << "\n";
display_graph(c_g);
cout << "Display out " <<  "\n";
			    if(!clash)
			    {    
				uvec_push(CowlObjectPtr, &stack, c_g);
			    } 
                       } // on (opers, op) again
                    }//IF contained
                }//IF
            } //loop on (n,c)
         }// each node of g
         
         free_graph(g);
         if(saturated) //if no node of "g" has a disjunction not processed, then "g" is OR saturated and clash-free 
         {
	    if(clash == false)
	    {
		cout << "SAT TRUE " <<"\n";
		return !clash;   
	    }else{
		cout << "SAT FALSE" << "\n";
		return !clash;
	    }
         }
   }//while
   //cout << "SAT FALSE 2" <<"\n";
   //return false;

    return !clash;
}

void test_satisfiable(const char* filename, const char* conceptname)
{
    //CowlClass* initConcept = cowl_class_from_static("http://www.ontology.com#A");
    
    Axioms_load* axioms_load = load_ontology(filename);
    cowl_init();
    // Instantiate a manager and deserialize an ontology from file.
    CowlManager *manager = cowl_manager();
    CowlOntology *ontology = cowl_manager_read_path(manager, ustring_wrap(filename, strlen(filename)));//filename
    cowl_release(manager); //added 8/11/2024
    
    if(axioms_load)
    {
	bool satisfiable =  is_satisfiable((CowlClsExp*) initConcept, axioms_load->axioms,  
		    axioms_load->genAxioms);

	if(satisfiable)
	    cout << "Concept satisfiable\n";
	else
	    cout << "Concept unsatisfiable\n";
    }else{
	cout << "There has been an error\n";
    }

}


