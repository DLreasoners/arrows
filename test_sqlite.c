
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
#include <pwd.h>
#include <sqlite3.h>

 int main()
 {
         sqlite3 *bd_index_fichiers;
 	 char *requete_sql=((char *)(malloc(8192)));
 	 char *db_err_msg=  (char*) malloc(1); //((char *)(NULL));
 	 int db_retval;
 	 db_retval = sqlite3_open("arrows", &bd_index_fichiers); 
 	 if (db_retval!=SQLITE_OK) 
         {
          fprintf(stderr, "Code Erreur SQL :  %d\n", db_retval); 
          fprintf(stderr, "Erreur SQL : 1 %s\n", db_err_msg); 
         } 
        strcpy(requete_sql,"CREATE TABLE IF NOT EXISTS file");
        strcat(requete_sql," (dirpath TEXT, filename TEXT, SHA1hash TEXT,");
        strcat(requete_sql," filesize INTEGER, mtime INTEGER, lvtime INTEGER,");
        strcat(requete_sql," PRIMARY KEY (dirpath, filename));");
        db_retval=sqlite3_exec(bd_index_fichiers,requete_sql,NULL,NULL,&db_err_msg);
        if (db_retval!=SQLITE_OK) 
        {
          fprintf(stderr, "Code Erreur SQL :  %d\n", db_retval); 
          fprintf(stderr, "Erreur SQL : 2 %s\n", db_err_msg); 
        }
        return 0;
 }   

 
