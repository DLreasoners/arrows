#ifndef NEGATIONNORMALFORM_H
#define NEGATIONNORMALFORM_H

#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

#include "cowl.h"

using namespace std;

CowlClsExp* nnf(CowlClsExp*, bool = false);
CowlClsExp* interUnionCase(CowlClsExp*, CowlObjectType, bool);




#endif
