#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <sqlite3.h>
#include "cowl.h"

using namespace std;

//extern UHash(CowlObjectTable) uhash_axioms;
//CowlClass* top,*bot;
typedef struct
{
	UHash(CowlObjectTable) *genAxioms;
	UHash(CowlObjectTable) *axioms;
	//CowlIRI *ontology_IRI;
} Axioms_load ;

Axioms_load* load_ontology(const char* filename);
void test_absorb();
void test_load();
CowlVector* absorb(CowlAny *axiom);
CowlClass*  getThing();
CowlClass*  getNothing();
//const char getIRI();
