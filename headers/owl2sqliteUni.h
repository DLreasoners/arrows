#ifndef NEGATIONNORMALFORM_H
#define NEGATIONNORMALFORM_H

#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <sqlite3.h>
#include "cowl.h"

using namespace std;

sqlite3* load_axioms_to_sqlite_uni(list<CowlAnyAxiom*> axs);

#endif
