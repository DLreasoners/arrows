 
#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <chrono>
#include <ratio>
#include <utility>
#include <sqlite3.h>
#include "cowl.h"

using namespace std;

CowlClsExp* cloningClsExp(CowlClsExp* ObjExp);
CowlClsExp* nnf(CowlClsExp*);
char* owl2manchester(CowlClsExp *cls);
char* owl2manchester(CowlAny *axiom);
/*
auto how_long = [](auto&& func, auto&&... params)
{
	const auto start = std::chrono::high_resolution_clock::now();
	std::forward<decltype(func)>(func)(std::forward<decltype(params)>(params)...);
    const auto stop = std::chrono::high_resolution_clock::now();
    const auto int_ms = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    return int_ms;
};

auto how_long_ret = [](auto&& func, auto&& ret, auto&&... params)
{
	const auto start = std::chrono::high_resolution_clock::now();
    *ret = std::forward<decltype(func)>(func)(std::forward<decltype(params)>(params)...);
    const auto stop = std::chrono::high_resolution_clock::now();
    const auto int_ms = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    return int_ms;
};
*/
void test_owl2manchester();
void test_owl2manchesterAxiom();
void test_nnf();
void test_absorb();
 
