#ifndef RESEARCH_H
#define RESEARCH_H

#include "cowl.h"

#include <algorithm>
#include <iostream>
#include <list>


// Arg1 : Label of a node, arg2 : concept to find
// return : true if the concept is in Label,
//	    false otherwise
bool checkConcept(list<CowlClsExp*>, CowlClsExp*);

// arg1 : Lable, arg2 : existential concept,
// return : an universal concept with the same role 
// if it is found, NULL otherwise
CowlObjQuant* checkUniversal(list<CowlClsExp*>, CowlObjQuant*);

// arg1 : head of nodes, arg2 : existential concept
bool checkFiller(Node*, CowlClsExp*);

/* arg1 : head of nodes, arg2 : existential concept, arg3 : universal concept
return :    true, if both fillers are found,
	    false, there are never both present at the same time.

Check if the fillers are both presents in the same label.
*/
bool checkFillers(Node*, CowlClsExp*, CowlClsExp*);












#endif
