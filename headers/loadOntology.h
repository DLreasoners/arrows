 
#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <sqlite3.h>
#include "cowl.h"

using namespace std;

//extern UHash(CowlObjectTable) uhash_axioms;

UHash(CowlObjectTable)* loadOntology(const char* filename);
void test_absorb();
void test_load();
CowlVector* absorb(CowlAny *axiom);
CowlClass*  getThing();
CowlClass*  getNothing();
