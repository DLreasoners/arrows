#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include "cowl.h"

#include <algorithm>
#include <iostream>
#include <list>


using namespace std;

class Node
{
    list<CowlClsExp*> Label;
public:
    Node* Next; 

    list<CowlClsExp*> getLabel();
    list<CowlClsExp*>::iterator getIterator();
    list<CowlClsExp*>::iterator getLast();
    void createLabel(list<CowlClsExp*>);
    void updateLabel(CowlClsExp*);

};

class Edge // Un objet par arète
{
    Node* parent;
    Node* child; 

    public:
    Edge(Node*, Node*);
};


class Graph
{
    Node** head;
    //Node* current;
    
    list<Edge>  edges;
    list<CowlClsExp*> formatted_axioms;
public:
    //constructor
    Graph(CowlClsExp*, list<CowlSubClsAxiom*>);

    // arg1 : fillers to add
    void new_node(Node*, CowlClsExp*, CowlClsExp*); //equivalent to push

    Node** getHead();
    //Node* getCurrent();
    list<CowlClsExp*> getAxioms();

    //Graph extend(Node*, list<CowlClsExp*>);
};


#endif
