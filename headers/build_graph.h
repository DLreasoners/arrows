#ifndef BUILD_GRAPH_H
#define BUILD_GRAPH_H

#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sqlite3.h>
#include "cowl.h"
#include "utils.h"

using namespace std;
 
typedef struct
{
	UVec(CowlObjectPtr)* nodes; //a set of concepts as CowlObjectPtr
	UHash(CowlObjectTable) *edges; //a set of pairs (key, value_1), ... , (key, value_n). This facilitates access to edges from the source of edges
} Graph;

/*
 
  "g" is saturated by all rules except for OR rule
  "currentNode" is the index of a node where a disjunction is detected
  "disj" is a disjunct of the disjunction
   It adds "disj" to the "currentNode" and applies all rules except for OR to the new graph and returns a saturated graph. 
   "axioms" is a set of absorbed axioms i.e subsumee is atomic. So it is needesd to add the subsumer whenever the subsumme occurs in the a node
*/

Graph* build_graph(Graph*, int, CowlClsExp*, bool*, UHash(CowlObjectTable)*, UHash(CowlObjectTable)*);

/*
    It applies all rules except for OR to "g" and returns a saturated graph.
    arg1 : g, the graph we're working on,
    arg2 : begin, an integer to know which node of the graph we need to work on after a disjunct has been added,
    arg3 : clash, boolean that declares the presence of a clash or not in the graph,
    arg4 : axioms, axioms in form of an hashtable for absorption,
    arg5 : genaxioms, general axiom that do not take part in the absorption process
*/
Graph* saturate_graph(Graph*, int, bool*, UHash(CowlObjectTable)*, UHash(CowlObjectTable)*);

/*
    Adds a new node to current graph in the presence of an existential and/or an universal object

    arg1 : g, the graph we're working on,
    arg2 : current_node, the node we're working on,
    arg3 : existential filler,
    arg4 : universal filler or NULL,
    arg5 : axioms, axioms in form of an hashtable for absorption,
    arg6 : genaxioms, general axiom that do not take part in the absorption process
*/
void create_node(Graph*, UVec(CowlObjectPtr)*, CowlClsExp*, CowlClsExp*, UHash(CowlObjectTable)*,UHash(CowlObjectTable)* );

/*
    function that triggers if we treat an existential object and no universal object with the same role name has been found,  
    to prepare for adding a node if needed 

    arg1 : g, the graph we're working on,
    arg2 : current_node, the node we're working on
    arg3 : existential filler,
    arg4 : axioms, axioms in form of an hashtable for absorption,
    arg5 : genaxioms, general axiom that do not take part in the absorption process
*/
void exists_adding(Graph*, UVec(CowlObjectPtr)*, CowlClsExp*, UHash(CowlObjectTable)*, UHash(CowlObjectTable)* );

/*
    function that triggers if we treat an existential object and at least one universal object with the same role name has been found,  
    to prepare for adding a node if needed 

    arg1 : g, the graph we're working on,
    arg2 : current_node, the node we're working on,
    arg3 : existential filler,
    arg5 : axioms, axioms in form of an hashtable for absorption,
    arg6 : genaxioms, general axiom that do not take part in the absorption process
    arg7 : the index of the associated universal object
*/
void exists_universal_adding(Graph*, UVec(CowlObjectPtr)*,CowlClsExp*, UHash(CowlObjectTable)*, UHash(CowlObjectTable)*, int);

/*
    function that triggers if we treat an universal object and at least one existential object with the same role name has been found,  
    to prepare for adding a node if needed 

    arg1 : g, the graph we're working on,
    arg2 : current_node, the node we're working on,
    arg3 : universal filler,
    arg5 : axioms, axioms in form of an hashtable for absorption,
    arg6 : genaxioms, general axiom that do not take part in the absorption process
    arg7 : the index of the associated existential object
*/
void universal_exists_adding(Graph*, UVec(CowlObjectPtr)*,CowlClsExp*, 
UHash(CowlObjectTable)*, int);

/*
    Check the presence of a clash in a specific node

    arg : node, the node we're working on
*/
bool check_clash(UVec(CowlObjectPtr)*);

/*
    To automatically add concepts that subsumed already present concept
    in res
    arg1 : axioms, arg2 : res
*/

void get_n_targets(UHash(CowlObjectTable)*, UVec(CowlObjectPtr)*);

/*
    To put general axioms in their correct forms
    arg1 : general axioms, arg2 : current node
*/
void format_genAxiom(UHash(CowlObjectTable)*, UVec(CowlObjectPtr)*);


/*
  To create a new graph from "source" without copying cOWL object targets 
*/
Graph* copy_graph(Graph* source);

/*
  Deallocate the memory of a graph.
  It deallocates just the vector "nodes" and the table "edges"
*/
void free_graph(Graph*);

// test functions
void test_saturate_graph(const char*);
void display_graph(Graph* g);
UHash(CowlObjectTable)* generate_axioms_bg_1();
UHash(CowlObjectTable)* generate_axioms_bg_2();

#endif
