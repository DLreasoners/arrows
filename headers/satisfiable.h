 

#include <algorithm>
#include <iostream>
#include <list>
#include <vector>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include "cowl.h"

using namespace std;

bool is_satisfiable(CowlClsExp* initConcept, UHash(CowlObjectTable)* axioms, UHash(CowlObjectTable)* genAxioms);

//test function 

void test_satisfiable(const char*);
