This is a Description Logic-based reasoner where the semantics of the Description Logic (DL) constructors is represented in categorical language.
The main difference between ARROWS and the  existing DL reasoners such as HermiT, Pellet, Konclude  is that these reasoners admit the usual set semantics relied on set operators with set membership while ARROWS is based  the categorial semantics with objects and arrows.

The implementation of ARROWS uses the library COWL available from https://github.com/sisinflab-swot/cowl
To install COWL under Linux, you can follow the instructions from https://swot.sisinflab.poliba.it/cowl/
Once you have installed COWL with dependencies under Linux, you should check the directory 
/usr/local/cowl/
where there are /usr/local/cowl/lib, /usr/local/cowl/docs, etc.

Assume that you download all files from ARROWS. To compile it from ARROWS, you type just

$ make (without $ of course)

If everything goes well, you obtain the executable arrows.o






 
