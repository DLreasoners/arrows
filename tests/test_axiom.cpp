#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

#include "cowl.h"
#include "negationNormalForm.h"

using namespace std;


list<CowlSubClsAxiom*> create_axioms()
{

    CowlClass* class_a = cowl_class_from_static("http://ontology.com#A");
    CowlClass* class_c = cowl_class_from_static("http://ontology.com#C");
    CowlClass* class_d = cowl_class_from_static("http://ontology.com#D");
    CowlClass* class_e = cowl_class_from_static("http://ontology.com#E");
    CowlClass* class_f = cowl_class_from_static("http://ontology.com#F");
    CowlClass* class_h = cowl_class_from_static("http://ontology.com#H");
    CowlClass* nothing = cowl_class_from_static("owl:Nothing");

    CowlObjProp* prop_r = cowl_obj_prop_from_static("http://ontology.com#R");

    // E sqcap F sqcap H
    UVec(CowlObjectPtr) ops_1 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_1, class_e);
    uvec_push(CowlObjectPtr, &ops_1, class_f);
    uvec_push(CowlObjectPtr, &ops_1, class_h);

    CowlVector* operands_1 = cowl_vector(&ops_1);

    CowlNAryBool* inter_EFH = cowl_nary_bool(COWL_NT_INTERSECT, operands_1);
    cowl_release(operands_1);

    // exist R.C, R.E
    CowlObjQuant* exists_RC = cowl_obj_quant(COWL_QT_SOME, prop_r , class_c);
    CowlObjQuant* exists_RE = cowl_obj_quant(COWL_QT_SOME, prop_r , class_e);

    // forall R.D, R.F, R.H

    CowlObjQuant* forall_RD = cowl_obj_quant(COWL_QT_SOME, prop_r , class_d);
    CowlObjQuant* forall_RE = cowl_obj_quant(COWL_QT_SOME, prop_r , class_e);
    CowlObjQuant* forall_RF = cowl_obj_quant(COWL_QT_SOME, prop_r , class_f);
    CowlObjQuant* forall_RH = cowl_obj_quant(COWL_QT_SOME, prop_r , class_h);

    // RC sqcap RD
    UVec(CowlObjectPtr) ops_2 = uvec(CowlObjectPtr);
    uvec_push(CowlObjectPtr, &ops_2, exists_RC);
    uvec_push(CowlObjectPtr, &ops_2, forall_RD);

    CowlVector* operands_2 = cowl_vector(&ops_2);
    CowlNAryBool* inter_RCRD = cowl_nary_bool(COWL_NT_INTERSECT, operands_2);
    cowl_release(operands_2);

    // Create axioms and push it in a list
    CowlSubClsAxiom* A_sub_interRCRD = cowl_sub_cls_axiom(class_a, inter_RCRD, NULL);
    CowlSubClsAxiom* D_sub_RE = cowl_sub_cls_axiom(class_d, forall_RE, NULL);
    CowlSubClsAxiom* D_sub_RF = cowl_sub_cls_axiom(class_d, forall_RF, NULL);
    CowlSubClsAxiom* D_sub_RH = cowl_sub_cls_axiom(class_d, forall_RH, NULL);
    CowlSubClsAxiom* inter_EFH_sub_bot = cowl_sub_cls_axiom(inter_EFH, nothing, NULL);



    list<CowlSubClsAxiom*> list_axiom = {
	    A_sub_interRCRD, 
	    D_sub_RE, 
	    D_sub_RF,
	    D_sub_RH, 
	    inter_EFH_sub_bot
    };

    return list_axiom;
}

CowlClsExp* nnf(CowlClsExp* ObjExp, bool neg)
{
    // If the class expression is a negation or coming from a negation
    int type = (int) cowl_get_type(ObjExp);

    // Negative branch
    if(type == COWL_OT_CE_OBJ_COMPL || neg == true)
    {
	// Depending on the type of the class expression,
	// we need to change the constructor
	switch(type){
	    case COWL_OT_CE_OBJ_COMPL:
	    {
		// If this is the first time we enter a negative branch or
		// if it comes from a positive branch
		if(!neg)
		{
		    return (CowlClsExp*) nnf(
			    cowl_obj_compl_get_operand((CowlObjCompl*) ObjExp), true);
		// We are on a negative branch, then if there is another negation, then 
		// it becomes a positive branch
		}else{
		    return (CowlClsExp*) nnf(
			    cowl_obj_compl_get_operand((CowlObjCompl*) ObjExp), false);
		} 
	    }
	    case COWL_OT_CE_OBJ_INTERSECT:	// conjunction
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_INTERSECT, true);
	    }
	    case COWL_OT_CE_OBJ_UNION:		// disjunction
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_UNION, true);
	    }
	    case COWL_OT_CE_OBJ_SOME:		// existential restriction  
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
		
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_ALL, role, 
						    nnf(filler, true));
		cowl_release(filler);
		cowl_release(role);

		return result;
	    }
	    case COWL_OT_CE_OBJ_ALL:		// universal restriction
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
	    
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_SOME, role, 
						    nnf(filler, true));
		cowl_release(filler);
		cowl_release(role);

		return result;
	    } 
	    case COWL_OT_CE_CLASS:	// Stop case in a negative branch, then we just
	    {				// return (CowlClsExp*) the negation of the axioms

		return (CowlClsExp*) cowl_obj_compl(ObjExp);
	    }
	    // DEBUG CASE ?
	    default:			// Not suppose to have any other types than those
	    {				// so we just return (CowlClsExp*) the guilty type and 
					// see what it is
		cout << "There's a problem" << "\n";
		cout << type << "\n";
		return NULL;
	    }
	}

    }else{

	// Positive Branch
	switch(type){
	    case COWL_OT_CE_OBJ_INTERSECT:
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_INTERSECT, false);
	    }
	    case COWL_OT_CE_OBJ_UNION:
	    {
		return interUnionCase(ObjExp, COWL_OT_CE_OBJ_UNION, false);
	    }
	    case COWL_OT_CE_OBJ_SOME:		// existential restriction  
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
	    
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_SOME, role, 
						    nnf(filler, false));
		cowl_release(filler);
		cowl_release(role);

		return result;
	    }
	    case COWL_OT_CE_OBJ_ALL:		// universal restriction
	    {
		CowlClsExp* filler = cowl_obj_quant_get_filler((CowlObjQuant*) ObjExp);
		CowlObjPropExp* role = cowl_obj_quant_get_prop((CowlObjQuant*) ObjExp);
	    
		CowlClsExp* result = (CowlClsExp*) cowl_obj_quant(COWL_QT_ALL, role, 
						    nnf(filler, false));
		cowl_release(filler);
		cowl_release(role);

		return result;
	    }
	    case COWL_OT_CE_CLASS:	// Stop case normally
	    {
		return (CowlClsExp*) ObjExp;// Since we are in a positive branch, we just 
	    }				    // return (CowlClsExp*) the axiom
	    // DEBUG MODE ?
	    default:			// Not suppose to have any other types than those
	    {				// so we just return (CowlClsExp*) the guilty type and 
					// see what it is
		cout << "There's a problem" << "\n";
		cout << type << "\n";
		return NULL;
	    }
	}

    } 
}

CowlClsExp* interUnionCase(CowlClsExp* ObjExp, CowlObjectType type, bool neg)
{
	// recovering all concepts/operands in one vector 
	CowlVector* operands_vector = 
			    cowl_nary_bool_get_operands((CowlNAryBool*) ObjExp);
	// recovering the number of concepts in the conjunction
	int nb_of_operands = (int) cowl_vector_count(operands_vector);
	UVec(CowlObjectPtr) new_ops = uvec(CowlObjectPtr);	

	// wraps all the operands in a vector
	// and call the recursive function on each operands
	for(int i = 0; i < nb_of_operands; i++)
	{ 

	    CowlClsExp* temp = (CowlClsExp*) cowl_vector_get_item(operands_vector, i);
	    uvec_push(CowlObjectPtr, &new_ops, nnf(temp, neg));
	    cowl_release(temp);
	}
	CowlVector* new_operands = cowl_vector(&new_ops);

	CowlNAryBool* result;
	
	if(neg == false){		    
	    // we are on a postive branch, we don't change the type
	    // of object
	    if(type == COWL_OT_CE_OBJ_UNION)
	    {	    
		result = cowl_nary_bool(COWL_NT_UNION, new_operands); 
	    }else{
		result = cowl_nary_bool(COWL_NT_INTERSECT, new_operands); 
	    }
	}else{
	    // we are on a negative branch, so we have to change the type based 
	    // on the orignal one
	    if(type == COWL_OT_CE_OBJ_UNION)
	    {	    
		result = cowl_nary_bool(COWL_NT_INTERSECT, new_operands); 
	    }else{
		result = cowl_nary_bool(COWL_NT_UNION, new_operands); 
	    }
	}   

	cowl_release(new_operands);

	return (CowlClsExp*) result;
}




int main(void)
{
    cowl_init();
    UOStream *std_out = uostream_std();

    list<CowlSubClsAxiom*> list_axiom = create_axioms();

    list<CowlClsExp*> list_axiom_nnf = {};

    list<CowlSubClsAxiom*>::iterator axiom_ptr = list_axiom.begin();

    while( axiom_ptr != list_axiom.end() )
    {

	CowlSubClsAxiom* current_axiom = *axiom_ptr; 

	// Recovering the upper class
	CowlClsExp* axiom_sup = nnf(cowl_sub_cls_axiom_get_super(current_axiom));
	//cout << "Le sup \n";
	//cowl_write_string(std_out, cowl_to_string(axiom_sup));	
	//cout << "\n";

	// Recovering the complement of the sub class in NNF
	CowlClsExp* axiom_sub_nnf = nnf((CowlClsExp*) cowl_obj_compl(
				    cowl_sub_cls_axiom_get_sub(current_axiom))
					);
	//cout << "Le sub \n";
	//cowl_write_string(std_out, cowl_to_string(axiom_sub_nnf));	
	//cout << "\n";
	

	// Creation of the axiom disjunction neg E sqcup F
	UVec(CowlObjectPtr) ops = uvec(CowlObjectPtr);
	uvec_push(CowlObjectPtr, &ops, axiom_sub_nnf);
	uvec_push(CowlObjectPtr, &ops, axiom_sup);

	CowlVector* operands = cowl_vector(&ops);

	list_axiom_nnf.push_back((CowlClsExp*) cowl_nary_bool(COWL_NT_UNION, operands));

	cowl_release(operands);

	axiom_ptr++;
    }
    cout << "liste de disjunction d'axiome : \n";

    list<CowlClsExp*>::iterator ptr = list_axiom_nnf.begin();
    while(ptr != list_axiom_nnf.end())
    {
	cowl_write_string(std_out, cowl_to_string(*ptr));
	cowl_write_static(std_out, "\n");
	ptr++;
    }
    
    return 0;
}










