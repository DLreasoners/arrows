COWL_DIR=/usr/local/cowl/cmake-build

all: bin/arrows
clean:
	rm -r bin/ 

bin/arrows.o:  src/arrows.cpp  src/negationNormalForm.cpp  src/utils.cpp src/load_ontology.cpp src/satisfiable.cpp src/build_graph.cpp 
	@mkdir -p bin
	g++ -c -I${COWL_DIR}/include -Iheaders src/negationNormalForm.cpp -o bin/negationNormalForm.o
	g++ -c -L/usr/include -I${COWL_DIR}/include -Iheaders   src/utils.cpp -o bin/utils.o
	g++ -c -L/usr/include -I${COWL_DIR}/include -Iheaders   src/load_ontology.cpp -o bin/load_ontology.o
	g++ -c -L/usr/include -I${COWL_DIR}/include -Iheaders   src/build_graph.cpp -o bin/build_graph.o
	g++ -c -L/usr/include -I${COWL_DIR}/include -Iheaders   src/satisfiable.cpp -o bin/satisfiable.o
	g++ -c -L/usr/include -I${COWL_DIR}/include -Iheaders   src/arrows.cpp -o bin/arrows.o
bin/arrows: bin/arrows.o   bin/utils.o bin/negationNormalForm.o  bin/load_ontology.o bin/build_graph.o bin/satisfiable.o 
	g++ -o bin/arrows bin/arrows.o bin/utils.o bin/negationNormalForm.o bin/load_ontology.o bin/satisfiable.o bin/build_graph.o ${COWL_DIR}/libcowl.a ${COWL_DIR}/lib/ulib/ulib.a

